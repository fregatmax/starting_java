/*
	Составить программу, которая определяет, есть ли хотя бы один элемент,
	равный заданному числу на побочной диагонали квадратного массива.
	В случае положительного ответа должны быть напечатаны координаты 
	любого из них.
*/

import java.util.Scanner;

class task12_185 {
	public static int[][] rndMatrix(int n, int m) {
		java.util.Random rn = new java.util.Random();
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++)  {
				ar[i][j] = rn.nextInt(20);	
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		return ar;
	}
	public static int[][] enterMatrix(int n, int m) {
		java.util.Scanner in = new java.util.Scanner(System.in);
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			System.out.print("Enter "+(i+1)+" row elements > ");
			for (int j=0; j<n; j++)
				ar[i][j] = in.nextInt();
			String temp = in.nextLine();
		}
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++) 
				System.out.printf("%3d ",ar[i][j]);
			System.out.println();
		}
		return ar;
	}
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter n (height and width) of 2D-array > ");
		final int n = in.nextInt();
		int ar[][] = enterMatrix(n,n);
		System.out.print("Enter a > ");
		int a = in.nextInt();
		for (int i=0; i<n; i++)
			if (ar[n-1-i][i] == a) {
				System.out.println("["+(n-i)+"]["+(i+1)+"] = "+a);
				return;
			}
		System.out.println("There is no such element on the secondary diagonal");
	}
}