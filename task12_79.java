/*
	Дан двумерный массив. Найти:
	а) сумму элементов в строках с k1-й по k2-ю;
	б) сумму элементов в столбцах с s1-го по s2-й.
*/

import java.util.Scanner;

class task12_79 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter n(width) and m(height) of 2D-array separated by space > ");
		final int n = in.nextInt(), m = in.nextInt();
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++) {
				ar[i][j] = (int)(Math.random()*10);	
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		System.out.println("\n----------------------------------------");
		System.out.print("Enter k1 and k2 > ");
		int k1 = in.nextInt(), k2 = in.nextInt(), sum = 0;
		if (k1 < k2)
			for (int i=(k1-1>=0?k1-1:0); i< (k2<m?k2:m); i++) 
				for (int j=0; j<n; j++) 
					sum += ar[i][j];
		System.out.println(sum);
		System.out.print("Enter s1 and s2 > ");
		int s1 = in.nextInt(), s2 = in.nextInt();
		sum = 0;
		if (s1 < s2)
			for (int j=(s1-1>=0?s1-1:0); j< (s2<n?s2:n); j++) 
				for (int i=0; i<m; i++) 
					sum += ar[i][j];
		System.out.println(sum);					
	}
}