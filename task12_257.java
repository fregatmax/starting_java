/*
	Дан двумерный массив из трех строк и восемнадцати строк столбцов. Переставить
	столбцы так, чтобы они располагались следующим способом: первый, 
	восемнадцатый, второй, семнадцатый, ..., восьмой, одиннадцатый, девятый, десятый.
*/

class task12_257 {
	public static int[][] rndMatrix(int n, int m) {
		java.util.Random rn = new java.util.Random();
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++)  {
				ar[i][j] = rn.nextInt(20);	
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		return ar;
	}
	public static void printMatrix(int[][] ar) {
		for (int i=0; i<ar.length; i++) {
			for (int j=0; j<ar[i].length; j++) 
				System.out.printf("%3d ",ar[i][j]);
			System.out.println();
		}
	}
	public static void main(String[] args) {
		final int n = 18, m = 3;
		int ar[][] = rndMatrix(n,m);
		int t[] = new int[m];
		for (int l=1; l<n; l+=2)
			for (int j=n-1; j>l; j--)
				for (int i=0; i<m; i++)
					ar[i][j] = ar[i][j-1] + ar[i][j] - (ar[i][j-1] = ar[i][j]);		
		System.out.println("-----------------");
		printMatrix(ar);
	}
}