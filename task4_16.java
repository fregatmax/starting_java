import java.util.Scanner;
class task4_16 {
public static void main(String[] args) {
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter circle and square areas separated by space > ");
	double c = stdin.nextDouble(), s = stdin.nextDouble();
	if(2*s<=c/Math.PI)	System.out.println("Square fits in circle");
	else if(s>=c/Math.PI)	System.out.println("Circle fits in square");
}
}