/*
Дан двумерный массив размером  n n , заполненный целыми числами. Вы-
яснить, является ли массив магическим квадратом. В магическом квадрате
суммы элементов по всем строкам, столбцам и двум диагоналям равны.
Значение, которому должны быть равны указанные суммы, определить са-
мостоятельно.
*/


import java.util.Scanner;
import java.util.Random;

class task12_131 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter n (size) of table > ");
		Random rn = new Random();
		final int n = in.nextInt();
		int ar[][] = new int[n][n];
		for (int i=0; i<n; i++) {
			for (int j=0; j<n; j++)  {
				ar[i][j] = rn.nextInt(10);	
				//ar[i][j] = in.nextInt();
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		boolean f = false;
		check: {
		// initialize num
		int num = 0, d2 = 0;
		//check diagonal
		for (int i=0; i<n;i++) {
			num += ar[i][i];
			d2 += ar[n-1-i][i];
		}
		if (num != d2)
			break check;
		// check rows
		for (int i=0; i<n; i++) {
			int sum = 0;
			for (int j=0; j<n; j++)
				sum += ar[i][j];
			if (sum != num)
				break check;
		}
		// check columns
		for (int j=0; j<n; j++) {
			int sum = 0;
			for (int i=0; i<n; i++)
				sum += ar[i][j];
			if (sum != num)
				break check;
		}
		f = true;
		}
		if (f)
			System.out.println("Magic square");
		else
			System.out.println("No magic square");
	}
}	