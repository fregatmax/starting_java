/*
	В массиве должна быть записана информация о максимальной скорости
	каждой из 30 моделей легковых автомобилей (в порядке возрастания). 
	После заполнения массива выяснилось, что значение k-го элемента 
	не соответствует требованию упорядоченности. изменить массив так,
	чтобы данные были упорядочены.
*/

import java.util.Scanner;
import java.util.Arrays;

class task11_177 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
//		System.out.print("Enter length of the array >");
		int len = 10;
		int ar[] = new int[len];
		System.out.print("Enter array elements > ");
		for (int i=0; i<len; i++) {
			ar[i] = in.nextInt();
		}
		System.out.print("Enter k > ");
		int k = in.nextInt()-1;
//		Arrays.sort(ar);
//		System.out.println();
		if (k != (ar.length-1) && ar[k] > ar[k+1])
			for (int i=k; i<ar.length; i++)
				if (ar[i+1] < ar[i])
					ar[i+1] = ar[i] + ar[i+1] - (ar[i] = ar[i+1]);
				else 
					break;
		else if (k != 0 && ar[k] < ar[k-1]) 
			for (int i=k; i>0; i--) 
				if (ar[i-1] > ar[i])
					ar[i-1] = ar[i] + ar[i-1] - (ar[i] = ar[i-1]);
				else 
					break;
		for (int i=0; i<len; i++) 
			System.out.print(ar[i]+" ");		
	}
}