// Выяснить, является ли заданное число n членом арифметической прогрессии,
// первый член которой равен f, а шаг — s.
import java.util.Scanner;

class task6_78{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter n, f, s > ");
	double n = stdin.nextDouble(), f = stdin.nextDouble(), s = stdin.nextDouble();
	if(s>0)
		while(f<n)	{
			f += s;
		}
	else
		while(f>n)	{
			f += s;
		}
	if(f==n) 	System.out.println("Belongs to the sequence");
	else		System.out.println("Doesn't belong to the sequence");
}
}