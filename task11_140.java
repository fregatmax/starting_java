/*
	В массиве хранится информация о среднедневной температуре за каждый
	день февраля. Определить даты двух самых холодных дней.
	
	Задачу решить, не используя два прохода по массиву.
*/

import java.util.Scanner;

class task11_140 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		// System.out.print("Enter length of the array >");
		int len = 28;
		int ar[] = new int[len];
		for (int i=0; i<ar.length; i++) {
			ar[i] = (int)(Math.random()*(-30));
			System.out.print(ar[i]+" ");
		}

		int m1 = 0, m2 = 1;
		for (int i=1; i<len; i++) 
			if (ar[i] < ar[m1]) {
				m2 = m1;
				m1 = i;
			}
			else if (ar[i] < ar[m2])
				m2 = i;
		System.out.println("\n"+(m1+1)+" "+(m2+1));
	}
}