import java.util.Scanner;

class task5_65{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	double area=0;
	for(int i=1; i<13; i++)	{
		switch(i) {
			case 1: System.out.println("1st district"); break;
			case 2: System.out.println("2nd district"); break;
			case 3: System.out.println("3rd district"); break;
			default: System.out.println(i+"st district");
		}
		System.out.print("Enterthe number of inhabitants (thousands) and population density (thousand/km^2) > ");
		double n = stdin.nextDouble(), d = stdin.nextDouble();
		area += n/d;
		}
		System.out.printf("Total area > %3.2f", area);
}
}