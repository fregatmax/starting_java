/*
	Дан двумерный массив.
	а) Переставить последнюю строку на место первой. При этом первую, 
	вторую, ..., предпоследнюю строки опустить.
	б) Переставить последний столбец на место первого. При этом первый, 
	второй, ..., предпоследний столбцы сместить вправо.
	в) Переставить s-ю строку на место k-й (s > k). При этом k-ю, (k + 1)-ю, ...,
	(s – 1)-ю строки опустить.
	г) Переставить a-й столбец на место b-го (a > b). При этом b-й, (b + 1)-й, ...,
	(a – 1)-й столбцы сместить вправо.
*/

import java.util.Scanner;
import java.util.Arrays;

class task12_253 {
	public static int[][] rndMatrix(int n, int m) {
		java.util.Random rn = new java.util.Random();
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++)  {
				ar[i][j] = rn.nextInt(20);	
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		return ar;
	}
	public static void printMatrix(int[][] ar) {
		for (int i=0; i<ar.length; i++) {
			for (int j=0; j<ar[i].length; j++) 
				System.out.printf("%3d ",ar[i][j]);
			System.out.println();
		}
	}
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter n(width) and m(height) of 2D-array separated by space > ");
		final int n = in.nextInt(), m = in.nextInt();
		int ar[][] = rndMatrix(n,m);
		int o = 0;
		do {
			System.out.print("\nEnter option (1..4), 0 - exit > ");
			o = in.nextInt();
			switch (o) {
	// а) Переставить последнюю строку на место первой. При этом первую, 
	// вторую, ..., предпоследнюю строки опустить.
				case 1:	{
					int t[] = Arrays.copyOf(ar[m-1],n);
					for (int i=0; i<m; i++)
						for (int j=0; j<n; j++)
							ar[i][j] = t[j] + ar[i][j] - (t[j] = ar[i][j]);		
					printMatrix(ar);
					}
					break;
	// б) Переставить последний столбец на место первого. При этом первый, 
	// второй, ..., предпоследний столбцы сместить вправо.
				case 2: {
					int t[] = new int[m];
					for (int i=0; i<m; i++)
						t[i] = ar[i][n-1];
					for (int j=0; j<n; j++)
						for (int i=0; i<m; i++)
							ar[i][j] = t[i] + ar[i][j] - (t[i] = ar[i][j]);
					printMatrix(ar);
					}
					break;
	// в) Переставить s-ю строку на место k-й (s > k). При этом k-ю, (k + 1)-ю, ...,
	// (s – 1)-ю строки опустить.
				case 3: {
					int s,k;
					do {
					System.out.print("Enter rows s, k (s>k) > ");
					s = in.nextInt()-1; k = in.nextInt()-1; // cause starting at 0
					} while(s < k);
					int t[] = Arrays.copyOf(ar[s],n);
					for (int i=k; i<=s; i++)
						for (int j=0; j<n; j++)
							ar[i][j] = t[j] + ar[i][j] - (t[j] = ar[i][j]);
					printMatrix(ar);
					}
					break;
	// г) Переставить a-й столбец на место b-го (a > b). При этом b-й, (b + 1)-й, ...,
	// (a – 1)-й столбцы сместить вправо.
				case 4: {
					int a,b;
					do {
					System.out.print("Enter columns a, b (a>b) > ");
					a = in.nextInt()-1; b = in.nextInt()-1; // cause starting at 0
					} while(a < b);
					int t[] = new int[m];
					for (int i=0; i<m; i++)
						t[i] = ar[i][a];
					for (int j=b; j<=a; j++)
						for (int i=0; i<m; i++)
							ar[i][j] = t[i] + ar[i][j] - (t[i] = ar[i][j]);
					printMatrix(ar);
					}
					break;
			}
		} while (o != 0);
	}
}