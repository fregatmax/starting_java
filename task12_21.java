/*
	Дан двумерный массив целых чисел. Выяснить:
	а) имеются ли нечетные числа в правом верхнем или в правом нижнем углу;
	б) имеются ли числа, оканчивающиеся цифрой 5, в левом верхнем или в
	левом нижнем углу.
*/

import java.util.Scanner;

class task12_21 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter n(width) and m(height) of 2D-array separated by space > ");
		int n = in.nextInt(), m = in.nextInt();
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++) {
				ar[i][j] = (int)(Math.random()*10+1);		
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		if (ar[0][n-1]%2 == 1 || ar[m-1][n-1]%2 == 1)
			System.out.println("Has odd number at top or bottom right corner");
		else
			System.out.println("Doesn't have odd number at top or bottom right corner");
		if (ar[0][0]%5 == 0 || ar[m-1][0]%5 == 0)
			System.out.println("Has number lasts with '5' at top or bottom left corner");
		else
			System.out.println("Doesn't have number lasts with '5' at top or bottom left corner");
		
	}
}