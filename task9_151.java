/*
	Дан текст. Найти максимальное из имеющихся в нем чисел.
*/
import java.util.Scanner;

class task9_151{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter text > ");
	String e = stdin.nextLine();
	int num = 0, max = 0;
	for (int i=0; i<e.length(); i++) {
		num = 0;
		while ( i < e.length() && e.charAt(i) > 47 && e.charAt(i) < 58) {
			num *= 10;
			num += Character.getNumericValue(e.charAt(i));
			i++;
		}
		if (num > max) {
			max = num;
			// System.out.print(num+" ");
		}
	}
	System.out.println();
	System.out.println(max);	
}
}