/*
	Составить программу вывода на экран любого элемента массива 
	по его индексу
*/

import java.util.Scanner;

class task11_13 {
	public static void main(String[] args) {
		int ar[] = new int[20];
		System.out.println("20 first numbers divisible by 13 and 17");
		for (int i=0, j=1; i<20; i++, j++) {
			for (; j<301; j++)
				if(j%13 == 0 | j%17 == 0)
					break;
			ar[i] = j;
			System.out.print(ar[i]+" ");
		}
		System.out.println("\n----------------------------------------");
		Scanner in = new Scanner(System.in);
		System.out.print("Enter n > ");
		System.out.print(ar[in.nextInt()-1]);
	}
}