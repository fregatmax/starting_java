/*
	изменить знак у максимального по модулю элемента массива. 
	Минимальный элемент массива при этом не определять.
*/

import java.util.Scanner;

class task11_142 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter length of the array >");
		int len = in.nextInt();
		int ar[] = new int[len];
		System.out.print("Enter array elements > ");
		for (int i=0; i<len; i++) {
			ar[i] = in.nextInt();
		}
		int m = 0;
		for (int i=1; i<len; i++) 
			if (Math.abs(ar[i]) > Math.abs(ar[m])) 
				m = i;
		ar[m] *= (-1);
		for (int i=0; i<len; i++) 
			System.out.print(ar[i]+" ");
	}
}