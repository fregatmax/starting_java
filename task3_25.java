class task3_25 {
public static void main(String[] args) {
	boolean A = false, B = false, C = false;
	System.out.println("A\tB\tC\t!(A&&B)&&(!A||!C)");
	for(int i=0;i<2;i++) {
		A = !A;
		for(int j=0;j<2;j++) {
			B = !B;
			for(int k=0;k<2;k++) {
				C = !C;
				System.out.println(A+"\t"+B+"\t"+C+"\t"+(!(A&&B)&&(!A||!C)));
			}
		}
	}
	System.out.println("-------------------------\nA\tB\tC\t!(A&&!B)||(A||!C)");
	for(int i=0;i<2;i++) {
		A = !A;
		for(int j=0;j<2;j++) {
			B = !B;
			for(int k=0;k<2;k++) {
				C = !C;
				System.out.println(A+"\t"+B+"\t"+C+"\t"+(!(A&&!B)||(A||!C)));
			}
		}
	}
	System.out.println("-------------------------\nA\tB\tC\tA&&!B||!(A||!C)");
	for(int i=0;i<2;i++) {
		A = !A;
		for(int j=0;j<2;j++) {
			B = !B;
			for(int k=0;k<2;k++) {
				C = !C;
				System.out.println(A+"\t"+B+"\t"+C+"\t"+(A&&!B||!(A||!C)));
			}
		}
	}		
}
}