/*
	В массиве хранится информация о баллах, полученных спортсменом-
	десятиборцем в каждом из десяти видов спорта. Для выхода в следующий
	этап соревнований общая сумма баллов должна превысить некоторое
	известное значение. Определить, вышел ли данный спортсмен в
	следующий этап соревнований.
*/
import java.util.Scanner;

class task11_33 {
	public static void main(String[] args) {
		int ar[] = new int[10];
		System.out.println("Points at each discipline");
		for (int i=0; i<ar.length; i++) {
			ar[i] = (int)(Math.random()*10);
			System.out.print(ar[i]+" ");
		}
		System.out.println("\n----------------------------------------");
		Scanner in = new Scanner(System.in);
		System.out.print("Enter points threshold > ");
		int c = in.nextInt(), sum = 0;
		for (int i=0; i<ar.length; i++)
			sum += ar[i];
//		System.out.println(sum);
		if (sum > c)
			System.out.print("Passed");
		else
			System.out.print("Failed");
	}
}
