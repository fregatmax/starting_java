 // Определить:
// а) является ли заданное число степенью числа 3;
// б) является ли заданное число степенью числа 5.
import java.util.Scanner;

class task6_76{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter n > ");
	int n = stdin.nextInt(), a=n;
	do	n /= 3;
	while (n%3.0==0);
	if(n==1) 	System.out.println("The number is a power of 3");
	else		System.out.println("the number is not a power of 3");
	
	do	a /= 5;
	while (a%5.0==0);
	if(a==1) 	System.out.println("The number is a power of 5");
	else		System.out.println("the number is not a power of 5");
}
}