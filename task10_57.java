/*
	Написать рекурсивную функцию, определяющую,
	является ли симметричной часть строки s, 
	начиная с i-го элемента и кончая j-м.
*/
import java.util.Scanner;

class task10_57{
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		System.out.print("Enter string > ");
		String s = in.next();
		System.out.print("Enter i and j > ");
		int i = in.nextInt()-1, j = in.nextInt()-1;
		System.out.print(isSym(s,i,j));
	}
	static boolean isSym(String s,int i, int j) {
		if( i > j )
			return true;
		return s.charAt(i) == s.charAt(j) ? isSym(s,++i,--j) : false;
	}
}