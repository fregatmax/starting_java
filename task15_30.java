/*
	имеются два текстовых файла с одинаковым числом строк. Переписать
	с сохранением порядка следования строки первого файла во второй, а строки
	второго файла — в первый. использовать вспомогательный файл.
*/

import java.util.Scanner;
import java.io.*;

class task15_30 {
	public static void main(String[] args)
	throws IOException
	{
		String temp;
		try {
			Scanner in = new Scanner(new File("task15_30_1.txt"));
			FileWriter fw = new FileWriter("task15_30_temp.txt");
			while (in.hasNextLine())
				fw.write(in.nextLine()+"\n");
			fw.close();
			fw = new FileWriter("task15_30_1.txt");
			in = new Scanner(new File("task15_30_2.txt"));
			while (in.hasNextLine()) 
				fw.write(in.nextLine()+"\n");
			fw.close();
			fw = new FileWriter("task15_30_2.txt");
			in = new Scanner(new File("task15_30_temp.txt"));
			while (in.hasNextLine()) 
				fw.write(in.nextLine()+"\n");
			fw.close();
		}
		catch (Exception e) { System.out.println(e); }
	}
}