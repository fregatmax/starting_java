import java.util.Scanner;

class task7_124{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter 20 numbers > ");
	double n = stdin.nextDouble(), a=n;
	int count = 0;
	for(int i=0;i<20;i++) {
		n = stdin.nextDouble();
		if(n!=a) count++;
		a = n;
	}
	System.out.println("Quantity of diverse numbers: "+count);
	System.out.println("Quantity of equal numbers: "+(20-count));
}
}