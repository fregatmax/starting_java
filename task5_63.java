﻿import java.util.Scanner;

class task5_63{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	double m=0, all=0;
	for(int i=1; i<11; i++)	{
		switch(i) {
			case 1: System.out.println("1st district"); break;
			case 2: System.out.println("2nd district"); break;
			case 3: System.out.println("3rd district"); break;
			default: System.out.println(i+"st district");
		}
		System.out.print("Enter the area (hectares) and yield (quintals per hectare) separated by space > ");
		double a = stdin.nextDouble(), y = stdin.nextDouble();
		all += a*y;
		m += y;
		}
		m /= 10;
		System.out.printf("Total gathered > %3.2f\tAverage yield > %3.2f", all,m);
}
}