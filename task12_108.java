/*
	Дан двумерный массив из двадцати двух строк и двух столбцов. Найти но-
	мера двух соседних строк, сумма элементов в которых максимальна.
*/

class task12_108 {
	public static void main(String[] args) {
		final int n = 2, m = 22;
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			System.out.print("["+(i+1)+"]\t");
			for (int j=0; j<n; j++) {
				ar[i][j] = (int)(Math.random()*10);	
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		System.out.println("\n----------------------------------------");
		int max = ar[0][0] + ar[0][1] + ar[1][0] + ar [1][1];
		int maxrow = 1;
		for (int i=2; i<m; i++) {
			int t = 0;
			for (int j=0; j<n; j++)
				t += ar[i][j]+ar[i-1][j];
			if (max < t) {
				max = t;
				maxrow = i;
			}
		}
		System.out.println("Max sum = "+max+" at rows "+maxrow+" and "+(maxrow+1));
	}
}		