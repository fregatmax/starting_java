/*
	Таблица футбольного чемпионата задана в виде двумерного массива из
	n строк и n столбцов, в котором все элементы, принадлежащие главной диа-
	гонали, равны нулю, а каждый элемент, не принадлежащий главной диаго-
	нали, равен 3, 1 или 0 (числу очков, набранных в игре: 3 — выигрыш, 1 —
	ничья, 0 — проигрыш).
	а) Найти число команд, имеющих больше побед, чем поражений.
	б) Определить номера команд, прошедших чемпионат без поражений.
	в) Выяснить, имеется ли хотя бы одна команда, выигравшая более полови-
	ны игр.
	г) Определить номер команды, ставшей чемпионом.
	д)* Определить, расположены ли команды в соответствии с занятыми ими
	местами в чемпионате (принять, что при равном числе очков места распре-
	деляются произвольно).
	е)* Для каждой команды определить занятое ею место (для простоты при-
	нять, что при равном числе очков места распределяются произвольно).
	ж)* Получить последовательность номеров команд в соответствии с заня-
	тыми ими местами (сначала должен идти номер команды, ставшей чемпио-
	ном, затем команды, занявшей второе место и т. д.).
*/

import java.util.Scanner;
import java.util.Random;

class task12_130 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter n (size) of table > ");
		Random rn = new Random();
		final int n = in.nextInt();
		int ar[][] = new int[n][n];
		for (int i=0; i<n; i++) {
			for (int j=0; j<i; j++) {					// get number in range [0;3)	
				int k = rn.nextInt(3);                  // we need 0,1 or 3 -> if k=2 then
				ar[i][j] = (k == 2 ? 3 : k);	 		//	we write 3
				ar[j][i] = ar[i][j] == 1 ? 1 : 3-ar[i][j];
			}										  	 
			ar[i][i] = 0;								
		}
		// drawing the table
		if (n>9) {
			System.out.print("        ");
			for (int i=0; i<9; i++)
				System.out.print("  ");
			for(int i=10; i<=n; i++)
				System.out.print((i/10)+" ");
		}
		System.out.print("\n________");
		for (int i=0; i<n; i++)
			System.out.print((i+1)%10+"_");
		System.out.println("\n");
		for (int i=0; i<n; i++) {
			System.out.printf("[%2d] -- ",(i+1));
			for (int j=0; j<n; j++)
				System.out.printf("%d ",ar[i][j]);
			System.out.println();
		}
		int o = 0;
		do {
			System.out.print("\nEnter option (1..6), 0 - exit > ");
			o = in.nextInt();
			switch (o) {
	//а) Найти число команд, имеющих больше побед, чем поражений.
				case 1:	{
					int count = 0;
					for (int i=0; i<n; i++) {
						int c = 1;
						for (int j=0; j<n; j++)
							if (ar[i][j] == 0 )
								c--;
							else if (ar[i][j] == 3)
								c++;
						if (c > 0)
							count++;
					}		
					System.out.println("Number of teams with more wins than losses - "+count);	
					}
					break;
	//б) Определить номера команд, прошедших чемпионат без поражений.
				case 2: {
					for (int i=0; i<n; i++) {
						boolean f = true;
						for (int j=0; j<n; j++)
							if (i != j & ar[i][j] == 0 )
								f = false;
						if (f)
							System.out.println("Team "+(i+1)+" never lost");
					}
					}
					break;
	//в) Выяснить, имеется ли хотя бы одна команда, выигравшая более половины игр.
				case 3: {
					boolean f = false;
					for (int i=0; i<n; i++) {
						int c = 1;
						for (int j=0; j<n; j++)
							if (ar[i][j] == 3)
								c++;
						if (c > n/2)
							f = true;
					}		
					if (f)
						System.out.println("At least one team has won more than half of the games");
					}
					break;
	//г) Определить номер команды, ставшей чемпионом.
				case 4: {
					int team = 0, sum = 0;
					for (int i=0; i<n; i++) {
						int t = 0;
						for (int j=0; j<n; j++)
							t += ar[i][j];
						if (t > sum) {
							team = i;
							sum = t;
						}
					}		
					System.out.println("Winner - "+(team+1));
					}
					break;
	// д)* Определить, расположены ли команды в соответствии с занятыми ими
	// местами в чемпионате (принять, что при равном числе очков места распределяются произвольно).
				case 5: {
					boolean f = true;
					int prevsum = n*3;
					first:for (int i=0; i<n; i++) {
						int t = 0;
						for (int j=0; j<n; j++)
							t += ar[i][j];
						if (t > prevsum) {
							f = false;
							break first;
						}
						else
							prevsum = t;
					}	
					if (f)
						System.out.println("Teams in the table are arranged according their points");
					else
						System.out.println("Teams in the table are not arranged according their points");
					}
					break;
	// е)* Для каждой команды определить занятое ею место (для простоты принять, что 
	// при равном числе очков места распределяются произвольно).
	// ж)* Получить последовательность номеров команд в соответствии с занятыми ими 
	// местами (сначала должен идти номер команды, ставшей чемпионом, затем команды, 
	// занявшей второе место и т. д.).
				case 6: {
					int max = 0, sum[] = new int[n];
					for (int i=0, count=0; i<n; i++, count++) {
						for (int j=0; j<n; j++)
							sum[count] += ar[i][j];
						if (sum[count] > max)
							max = sum[count];
					}
					int count = 1, winorder[] = new int[n];
					do {
						int newmax = 0;
						for (int i=0; i<n; i++)
							if (sum[i] == max) {
								winorder[count-1] = i+1;
								System.out.printf("[%2d] - %2d team with sum of points %d\n",count++,(i+1),sum[i]);
							}
							else if ( sum[i] > newmax & sum[i] < max)
								newmax = sum[i];
						max = newmax;
					} while (count <= n);
					}
					break;
			}
		} while (o != 0);
	}
}