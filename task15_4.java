/*
	Дан массив строк. Записать их в файл, расположив каждый элемент массива
	на отдельной строке с сохранением порядка.
*/

import java.io.*;
import java.util.Scanner;
 
public class task15_4 {
    public static void main(String[] args) 
		throws IOException
	{  
		Scanner in = new Scanner(System.in);
		System.out.print("Enter number of strings > ");
		int n = in.nextInt();
		System.out.print("Enter sentences (end with enter) > ");
		String str[] = new String[n], temp = in.nextLine();
		for (int i=0; i<n; i++)
			str[i] = in.nextLine();
		try{
			FileWriter fw = new FileWriter("task15_4.txt");
			for (int i=0; i<n; i++)
				fw.write(str[i]+"\n");
			fw.close();
		}
		catch(Exception e){System.out.println(e);}
    } 
}