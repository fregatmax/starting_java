/*
	Создать текстовый файл и записать в него 6 строк. Записываемые строки вво-
	дятся с клавиатуры.
*/

import java.io.*;
import java.util.Scanner;
 
public class task15_3 {
    public static void main(String[] args) 
		throws IOException
	{  
		Scanner in = new Scanner(System.in);
		try{
			FileWriter fw = new FileWriter("task15_3.txt");
			System.out.print("Enter 6 sentences (end with enter) > ");
			for (int i=0; i<6; i++)
				fw.write(in.nextLine()+"\n");
			fw.close();
		}
		catch(Exception e){System.out.println(e);}
    } 
}