import java.util.Scanner;

class task6_45{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter a,b,c separated by spaces > ");
	double a = stdin.nextDouble(), b = stdin.nextDouble(), c = stdin.nextDouble();
	while (a!=b)
		if (a>b)	a = a - b;
		else		b = b - a;
	while (c!=b)
		if (c>b)	c = c - b;
		else		b = b - c;
	System.out.println(c);
}
}