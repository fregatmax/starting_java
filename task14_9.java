/*
	В конец существующего файла записать:
	а) 6 целых чисел;
	б) 5 вещественных чисел;
	в) 4 слова.
	Записываемые числа и слова вводятся с клавиатуры.
*/

import java.io.*;
import java.util.Scanner;
 
public class task14_9 {
    public static void main(String[] args) 
		throws IOException
	{  
		int num = 0;
		Scanner in = new Scanner(System.in);
		do {
		System.out.print("Enter option (1,2 or 3) > ");
        num = in.nextInt();
		} while (num != 1 & num != 2 & num != 3);
		try{
			FileWriter fw = new FileWriter("task14_9.txt", true);
			switch (num) {
				case 1:
					System.out.print("Enter 6 integers > ");
					for (int i=0; i<6; i++)
						fw.write(" "+in.nextInt());
					break;
				case 2:
					System.out.print("Enter 5 real numbers > ");
					for (int i=0; i<5; i++)
						fw.write(" "+in.nextDouble());
					break;
				case 3:
					System.out.print("Enter 4 words > ");
					for (int i=0; i<4; i++)
						fw.write(" "+in.next());
					break;
			}
			fw.close();
		}
		catch(Exception e){System.out.println(e);}
    } 
}