/*
	Дан двумерный массив. Найти:
	а) число пар одинаковых соседних элементов в каждой строке;
	б) число пар одинаковых соседних элементов в каждом столбце.
*/

import java.util.Scanner;

class task12_78 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter n(width) and m(height) of 2D-array separated by space > ");
		final int n = in.nextInt(), m = in.nextInt();
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++) {
				ar[i][j] = (int)(Math.random()*10);	
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		System.out.println("\n----------------------------------------");
		int pairs = 0;
		for (int i=0; i<m; i++){
			pairs = 0;
			for (int j=1; j<n; j++) 
				if (ar[i][j-1] == ar[i][j])
					pairs++;
			System.out.println("Pairs at "+(i+1)+" row = "+pairs);
		}
		for (int j=0; j<n; j++){ 
			pairs = 0;
			for (int i=1; i<m; i++) 
				if (ar[i-1][j] == ar[i][j])
					pairs++;
			System.out.println("Pairs at "+(j+1)+" column = "+pairs);
		}
	}
}