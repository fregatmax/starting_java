/*
	В одномерном массиве имеются только два одинаковых элемента. Найти их.
*/

import java.util.Scanner;

class task11_102 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter length of the array >");
		int len = in.nextInt();
		int ar[] = new int[len];
		System.out.print("Enter array elements > ");
		for (int i=0; i<len; i++) {
			ar[i] = in.nextInt();
		}
		int el = 0;
		for (int i=0; i<len; i++) 
			for (int j=i+1; j<len; j++)
				if (ar[i] == ar[j])
					el = ar[i];
		System.out.println(el);
	}
}