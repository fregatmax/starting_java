class task3_22 {
public static void main(String[] args) {
	boolean X = false, Y = false, Z = false;
	System.out.println("X\tY\tZ\t!(X||!Y&&Z)");
	for(int i=0;i<2;i++) {
		X = !X;
		for(int j=0;j<2;j++) {
			Y = !Y;
			for(int k=0;k<2;k++) {
				Z = !Z;
				System.out.println(X+"\t"+Y+"\t"+Z+"\t"+(!(X||!Y&&Z)));
			}
		}
	}
	System.out.println("-------------------------\nX\tY\tZ\tY||(X&&!Y||Z)");
	for(int i=0;i<2;i++) {
		X = !X;
		for(int j=0;j<2;j++) {
			Y = !Y;
			for(int k=0;k<2;k++) {
				Z = !Z;
				System.out.println(X+"\t"+Y+"\t"+Z+"\t"+(Y||(X&&!Y||Z)));
			}
		}
	}
	System.out.println("-------------------------\nX\tY\tZ\t!(!X&&Y||Z)");
	for(int i=0;i<2;i++) {
		X = !X;
		for(int j=0;j<2;j++) {
			Y = !Y;
			for(int k=0;k<2;k++) {
				Z = !Z;
				System.out.println(X+"\t"+Y+"\t"+Z+"\t"+(!(!X&&Y||Z)));
			}
		}
	}		
}
}