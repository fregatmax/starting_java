/*
	Составить программу:
	а) сравнения по абсолютной величине элемента, расположенного в верхнем
	правом углу двумерного массива, с любым другим элементом массива 
	(определить, какая из абсолютных величин больше);
	б) сравнения двух любых элементов массива (определить, какой из них	меньше).
*/

import java.util.Scanner;

class task12_19 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter [m][n] (heigth and width) of 2D-array separated by space > ");
		final int n = in.nextInt(), m = in.nextInt();
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++) {
				ar[i][j] = (int)((Math.random()-0.5)*20);	
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		System.out.print("Enter height and width of element to compare with top right > ");
		int n1 = in.nextInt(), m1 = in.nextInt();
		if (Math.abs(ar[m1-1][n1-1]) > Math.abs(ar[0][n-1]))
			System.out.println("ar ["+m1+"]["+n1+"] bigger");
		else
			System.out.println("ar ["+1+"]["+n+"] bigger");
		System.out.print("Enter height and width of first element> ");
		n1 = in.nextInt(); m1 = in.nextInt();
		System.out.print("Enter height and width of second element> ");
		int n2 = in.nextInt(), m2 = in.nextInt();
		if (ar[m1-1][n1-1] < ar[m2-1][n2-1])
			System.out.println("ar["+m1+"]["+n1+"] less");
		else
			System.out.println("ar["+m2+"]["+n2+"] less");
	}
}