/*
	Даны два предложения. Найти общее количество букв н в них. (Определить
	функцию для расчета количества букв н в предложении.)
*/
import java.util.Scanner;

class task10_28{
	public static void main(String[] args){
		Scanner stdin = new Scanner(System.in);
		String s[] = new String[2];
		for (int i=0; i<2; i++) {
			System.out.print("Enter sentence > ");
			s[i] = stdin.nextLine();
		}
		System.out.print("Total amount of letters \"n\" in sentences - ");
		System.out.print(amountOfN(s[0])+amountOfN(s[1]));	
	}
	static int amountOfN(String s) {
		int r = 0;
		for (int i=0; i<s.length(); i++)
			if (s.charAt(i) == 'n')
				r++;
		return r;
		
	}
}
