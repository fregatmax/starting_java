/*
	В массиве хранится информация о массе каждого из 30 предметов,
	загружаемых в грузовой автомобиль, грузоподъемность которого известна.
	Определить, не превышает ли общая масса всех предметов
	грузоподъемность автомобиля.
*/
import java.util.Scanner;

class task11_32 {
	public static void main(String[] args) {
		int ar[] = new int[30];
		System.out.println("Weigth of 30 random things (each < 10)");
		for (int i=0; i<ar.length; i++) {
			ar[i] = (int)(Math.random()*10)+1;
			System.out.print(ar[i]+" ");
		}
		System.out.println("\n----------------------------------------");
		Scanner in = new Scanner(System.in);
		System.out.print("Enter cargo capicity > ");
		int c = in.nextInt(), sum = 0;
		for (int i=0; i<ar.length; i++)
			sum += ar[i];
//		System.out.println(sum);
		if (sum > c)
			System.out.print("Overload");
		else
			System.out.print("Ok");
	}
}
