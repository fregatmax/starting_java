import java.util.Scanner;

class task9_74{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter sentence > ");
	String s = stdin.nextLine();
	char t = s.charAt(0);
	int f = 1;
	for(int i=1;i<s.length();i++) {
		if(f == 5)	break;
		if(t == s.charAt(i))
			f++;
		else
			t = s.charAt(i);
	}
	if(f == 5)	
		System.out.println("In the text there are 5 identical symbols in a row");
	else	
		System.out.println("In the text there are not 5 identical symbols in a row");
}
}