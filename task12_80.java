/*
	Дан двумерный массив. Определить количество различных элементов в нем.
*/

import java.util.Scanner;

class task12_80 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter n(width) and m(height) of 2D-array separated by space > ");
		final int n = in.nextInt(), m = in.nextInt();
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++) {
				ar[i][j] = (int)(Math.random()*10);	
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		System.out.println("\n----------------------------------------");
		int count = 0;
		for (int i=0; i<m; i++)
			count:for (int j=0; j<n; j++) {
				for (int k=i; k>=0; k--)
					for (int l =(k == i ? j : n-1); l>=0; l--)
						if ((i != k | j != l) & ar[i][j] == ar[k][l])
							continue count;
				count++;
			}
		System.out.println(count);
	}
}