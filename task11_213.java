/*
	известны оценки по информатике каждого ученика класса. В начале списка
	перечислены все пятерки, затем все остальные оценки. Сколько учеников
	имеют по информатике оценку "5"? Рассмотреть возможность случая, что
	такую оценку имеют все ученики. Условный оператор не использовать.
*/

import java.util.Scanner;

class task11_213 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter length of the array >");
		int len = in.nextInt();
		int ar[] = new int[len];
		System.out.print("Enter array elements > ");
		for (int i=0; i<len; i++) {
			ar[i] = in.nextInt();
		}
		int i = 0;
		while (i != ar.length-1 & ar[i] == 5) 
			i++;
		if (i != 0)
			System.out.println("Number of exellent pupils - "+i);
	}
}