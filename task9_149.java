/*
	Дан текст, в котором имеется несколько идущих подряд цифр. 
	Получить число, образованное этими цифрами.
*/
import java.util.Scanner;

class task9_149{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter text > ");
	String e = stdin.nextLine();
	int num = 0;
	for (int i=0; i<e.length(); i++) {
		while (i < e.length() && e.charAt(i) > 47 && e.charAt(i) < 58) {
			num *= 10;
			num += Character.getNumericValue(e.charAt(i));
			i++;
			System.out.println(num);
		}
	}
	System.out.println(num);	
}
}