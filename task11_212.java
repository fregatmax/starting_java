/*
	В начале массива записаны несколько равных между собой элементов. 
	Определить количество таких элементов и вывести все элементы,
	следующие за последним из них. Рассмотреть возможность того, что
	весь массив заполнен одинаковыми элементами. 
	Условный оператор не использовать.
*/

import java.util.Scanner;

class task11_212 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter length of the array >");
		int len = in.nextInt();
		int ar[] = new int[len];
		System.out.print("Enter array elements > ");
		for (int i=0; i<len; i++) {
			ar[i] = in.nextInt();
		}
		int i = 0;
		while (i++ != ar.length-1 & ar[i] == ar[i-1]);
		System.out.println("Number of same elements - "+i);
		while (i != ar.length)
			System.out.print(ar[i++]+" ");
		
	}
}