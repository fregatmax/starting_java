import java.util.Scanner;

class task6_85{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter n > ");
	int n = stdin.nextInt(), order = 0;		// order = 1 "2" to the left of "5"
	while(n>=10)	{						// order = 2 "5" to the left of "2"
		n /= 10;
		if(n%10==2)	order = 1;
		if(n%10==5)	order = 2;
	}
	if(order==1)	
		System.out.println("2 to left of 5");
	else 
		System.out.println("5 to left of 2");
}
}