/*
	имеется текстовый файл. Переписать в другой файл те его строки, в которых
	имеется более 30-ти символов.
*/

import java.util.Scanner;
import java.io.*;

class task15_27 {
	public static void main(String[] args)
	throws IOException
	{
		String temp;
		try {
			Scanner in = new Scanner(new File("task15_27_in.txt"));
			FileWriter fw = new FileWriter("task15_27_out.txt");
			while (in.hasNextLine()) {
				temp = in.nextLine();
				if (temp.length() > 30)
					fw.write(temp+"\n");
			}
			fw.close();
		}
		catch (Exception e) { System.out.println(e); }
	}
}