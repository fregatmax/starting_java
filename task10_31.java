/*
	Даны два предложения, в которых имеются буквы ш. Найти, в каком из них
	эта буква имеет больший порядковый номер (при счете от начала предложения).
	Если в предложении имеется несколько букв s, то должна быть учтена
	последняя из них. (Определить функцию для нахождения порядкового номера
	буквы последнего вхождения в предложение некоторой буквы.)
*/
import java.util.Scanner;

class task10_31{
	public static void main(String[] args){
		Scanner stdin = new Scanner(System.in);
		String s[] = new String[2];
		for (int i=0; i<2; i++) {
			System.out.print("Enter sentence > ");
			s[i] = stdin.nextLine();
		}
		if (placeOfS(s[0]) > placeOfS(s[1]))
			System.out.print("The place of last \"s\" in first sentence is bigger");
		else 
			System.out.print("The place of last \"s\" in second sentence is bigger");
	}
	static int placeOfS(String s) {
		int r = 0;
		for (int i=0; i<s.length(); i++)
			if (s.charAt(i) == 's')
				r = i;
		return r;
	}
}