import java.util.Scanner;

class task5_64{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	double area=0, human=0,m;
	for(int i=1; i<13; i++)	{
		switch(i) {
			case 1: System.out.println("1st district"); break;
			case 2: System.out.println("2nd district"); break;
			case 3: System.out.println("3rd district"); break;
			default: System.out.println(i+"st district");
		}
		System.out.print("Enterthe number of inhabitants (thousands) and the area (km^2) > ");
		double n = stdin.nextDouble(), a = stdin.nextDouble();
		area += a;
		human += n;
		}
		m = human/area;
		System.out.printf("Average population density > %3.2f", m);
}
}