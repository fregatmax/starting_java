/*
	имеется текстовый файл. Удалить из него первую строку, в конце которой
	стоит вопросительный знак. Результат записать в другой файл.
*/

import java.io.*;
import java.util.Scanner;
 
public class task15_13 {
    public static void main(String[] args) 
		throws IOException
	{  
		String temp;
		boolean fl = false;
		try{
			Scanner in = new Scanner(new File("task15_13_in.txt"));
			FileWriter fw = new FileWriter("task15_13_out.txt");
			while (in.hasNextLine()) {
				temp = in.nextLine();
				if (!temp.endsWith("!") | fl) {
					System.out.println(temp);
					fw.write(temp+"\n");
				}
				else {
					fl = true;
				}
			}
			fw.close();
		}
		catch(Exception e){System.out.println(e);}
    }
}