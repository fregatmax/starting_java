/*
	Дан двумерный массив целых чисел. Определить:
	а) есть ли в нем строка, состоящая только из нечетных элементов;
	б) есть ли в нем строка, состоящая только из элементов, кратных числу a или b;
	в) есть ли в нем строка, состоящая только из отрицательных элементов;
	г) есть ли в нем строка, содержащая больше положительных элементов, чем
	отрицательных;
	д) есть ли в нем строка, в которой имеются одинаковые элементы;
	е) есть ли в нем строка, в которой имеются как минимум два элемента, яв-
	ляющиеся максимальными в массив
*/

import java.util.Scanner;

class task12_152 {
	public static int[][] rndMatrix(int n, int m) {
		java.util.Random rn = new java.util.Random();
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++)  {
				ar[i][j] = rn.nextInt(20);	
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		return ar;
	}
	public static int[][] enterMatrix(int n, int m) {
		java.util.Scanner in = new java.util.Scanner(System.in);
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			System.out.print("Enter "+(i+1)+" row elements > ");
			for (int j=0; j<n; j++)
				ar[i][j] = in.nextInt();
			String temp = in.nextLine();
		}
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++) 
				System.out.printf("%3d ",ar[i][j]);
			System.out.println();
		}
		return ar;
	}
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter n(width) and m(height) of 2D-array separated by space > ");
		final int n = in.nextInt(), m = in.nextInt();
		//int ar[][] = rndMatrix(n,m);
		int ar[][] = enterMatrix(n,m);
		int o = 0;
		do {
			System.out.print("\nEnter option (1..6), 0 - exit > ");
			o = in.nextInt();
			switch (o) {
	//а) а) есть ли в нем строка, состоящая только из нечетных элементов;
				case 1:	{
					boolean f =false;
					for (int i=0; i<m; i++) {
						boolean fl = true;
						for (int j=0; j<n; j++)
							fl = ar[i][j]%2 == 1 ? fl : false;
						if (fl)
							f =true;
					}		
					if (f)
						System.out.println("There is a row consisting only odd numbers");
					else
						System.out.println("There is not a row consisting only odd numbers");
					}
					break;
	//б)  есть ли в нем строка, состоящая только из элементов, кратных числу a или b;
				case 2: {
					System.out.print("Enter a and b > ");
					int a = in.nextInt(), b = in.nextInt();
					boolean f = false;
					for (int i=0; i<m; i++) {
						boolean fl = true;
						for (int j=0; j<n; j++)
							fl = (ar[i][j]%a == 0 || ar[i][j]%b == 0) ? fl : false;
						if (fl)
							f =true;
					}		
					if (f)
						System.out.println("There is a row of elements divisible by "+a+" or "+b);
					else
						System.out.println("There is not a row of elements divisible by "+a+" or "+b);
					}
					break;
	//в) есть ли в нем строка, состоящая только из отрицательных элементов;
				case 3: {
					boolean f = false;
					for (int i=0; i<m; i++) {
						boolean fl = true;
						for (int j=0; j<n; j++)
							fl = ar[i][j] < 0 ? fl : false;
						if (fl)
							f =true;
					}		
					if (f)
						System.out.println("There is a row consisting only negative numbers");
					else
						System.out.println("There is not a row consisting only negative numbers");
					}
					break;
	// г) есть ли в нем строка, содержащая больше положительных элементов, чем
	//  отрицательных;
				case 4: {
					boolean f = false;
					for (int i=0; i<m; i++) {
						int c = 0;
						for (int j=0; j<n; j++)
							if (ar[i][j] != 0)
								c = ar[i][j] > 0 ? c+1 : c-1;
						if (c > 0)
							f =true;
					}		
					if (f)
						System.out.println("There is a row consisting more positive elements than negative");
					else
						System.out.println("There is not a row consisting more positive elements than negative");
					}
					break;
	// д) есть ли в нем строка, в которой имеются одинаковые элементы;
				case 5: {
					boolean f = false;
					five:for (int i=0; i<m; i++)
						for (int j=0; j<n; j++)
							for(int k=j+1; k<n; k++)	
								if (ar[i][j] == ar[k][j]) {
									f =true;
									break five;
								}
					if (f)
						System.out.println("There is a row consisting same elements");
					else
						System.out.println("There is not a row consisting same elements");
					}
					break;
	// е) есть ли в нем строка, в которой имеются как минимум два элемента,
	// являющиеся максимальными в массиве
				case 6: {
					boolean f = false;
					int max = ar[0][0];
					for (int i=0; i<m; i++)
						for (int j=0; j<n; j++)
							max = max < ar[i][j] ? ar[i][j] : max;
					for (int i=0; i<m; i++) {
						int c =0;
						for (int j=0; j<n; j++)
							c = ar[i][j] == max ? c+1 : c;
						if (c > 1) 
								f =true;
					}
					if (f)
						System.out.println("There is a row consisting 2 and more maximum elements");
					else
						System.out.println("There is not a column consisting 2 and more maximum elements");
					}
					break;
			}
		} while (o != 0);
	}
}