class task3_23 {
public static void main(String[] args) {
	boolean A = false, B = false, C = false;
	System.out.println("A\tB\tC\t!(A||!B&&C)||C");
	for(int i=0;i<2;i++) {
		A = !A;
		for(int j=0;j<2;j++) {
			B = !B;
			for(int k=0;k<2;k++) {
				C = !C;
				System.out.println(A+"\t"+B+"\t"+C+"\t"+(!(A||!B&&C)||C));
			}
		}
	}
	System.out.println("-------------------------\nA\tB\tC\t!(A&&!B||C)&&B");
	for(int i=0;i<2;i++) {
		A = !A;
		for(int j=0;j<2;j++) {
			B = !B;
			for(int k=0;k<2;k++) {
				C = !C;
				System.out.println(A+"\t"+B+"\t"+C+"\t"+(!(A&&!B||C)&&B));
			}
		}
	}
	System.out.println("-------------------------\nA\tB\tC\t!(!A||B&&C)||A");
	for(int i=0;i<2;i++) {
		A = !A;
		for(int j=0;j<2;j++) {
			B = !B;
			for(int k=0;k<2;k++) {
				C = !C;
				System.out.println(A+"\t"+B+"\t"+C+"\t"+(!(!A||B&&C)||A));
			}
		}
	}		
}
}