/*
	Дан одномерный массив m из 12 элементов. Переставить его элементы
	следующим способом:  1 , m 12 , m 2 , m 11 , m ...,  5 , m 8 , m 6 , m 7 
*/

class task11_179 {
	public static void main(String[] args) {
		int len = 12;
		int ar[] = new int[len];
		for (int i=0; i<ar.length; i++) {
			// ar[i] = (int)(Math.random()*(30));
			ar[i] = i+1;
			System.out.print(ar[i]+" ");
		}
		for (int j=1; j<=ar.length; j+=2)
			for (int i=ar.length-1; i>j; i--)
				ar[i] = ar[i-1] + ar[i] - (ar[i-1] = ar[i]);
		System.out.println();
		for (int i=0; i<len; i++) 
			System.out.print(ar[i]+" ");
	}
}