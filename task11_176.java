/*
	В массиве должна быть записана информация о количестве жителей в 
	каждом из 30 городов (в порядке возрастания численности). После 
	заполнения массива выяснилось, что значение последнего элемента
	не соответствует требованию упорядоченности. изменить массив так,
	чтобы данные были упорядочены.
*/

import java.util.Scanner;
import java.util.Arrays;

class task11_176 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter length of the array >");
		int len = in.nextInt();
		int ar[] = new int[len];
		System.out.print("Enter array elements > ");
		for (int i=0; i<len; i++) {
			ar[i] = in.nextInt();
		}
		Arrays.sort(ar);
		System.out.println();
		for (int i=0; i<len; i++) 
			System.out.print(ar[i]+" ");
	}
}