/*
	Дан квадратный массив целых чисел. Определить, является ли 
	он симметричным относительно своей главной диагонали.
*/


import java.util.Scanner;

class task12_188 {
	public static int[][] rndMatrix(int n, int m) {
		java.util.Random rn = new java.util.Random();
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++)  {
				ar[i][j] = rn.nextInt(20);	
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		return ar;
	}
	public static int[][] enterMatrix(int n, int m) {
		java.util.Scanner in = new java.util.Scanner(System.in);
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			System.out.print("Enter "+(i+1)+" row elements > ");
			for (int j=0; j<n; j++)
				ar[i][j] = in.nextInt();
			String temp = in.nextLine();
		}
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++) 
				System.out.printf("%3d ",ar[i][j]);
			System.out.println();
		}
		return ar;
	}
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter n (height and width) of 2D-array > ");
		final int n = in.nextInt();
		int ar[][] = enterMatrix(n,n);
		for (int i=0; i<n; i++)
			for(int j=i+1; j<n; j++)
				if (ar[i][j] != ar[j][i]) {
					System.out.println("Matrix is not symmetric with respect to the main diagonal");
					return;
			}
		System.out.println("Matrix is symmetric with respect to the main diagonal");
	}
}