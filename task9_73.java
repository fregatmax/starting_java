import java.util.Scanner;

class task9_73{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter sentence > ");
	String s = stdin.nextLine();
	if(s.lastIndexOf('s') > s.lastIndexOf('t'))	
		System.out.println("letter 's' occurs later than 't'");
	else	
		System.out.println("letter 't' occurs later than 's'");
}
}