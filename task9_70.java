import java.util.Scanner;

class task9_70{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter sentence > ");
	String s = stdin.nextLine();
	int vowel = 0;
	for(int i=0;i<s.length();i++)
		switch(s.charAt(i)){
			case 'a':
			case 'e':
			case 'i':
			case 'o':
			case 'u':
//			case 'y':
			case 'A':
			case 'E':
			case 'I':
			case 'O':
			case 'U':
//			case 'Y': 
				vowel++;
				break;
		}
	System.out.println("Number of vowels in sentence: "+vowel);
}
}