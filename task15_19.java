/*
	имеется текстовый файл. Найти:
	а) количество строк, начинающихся с букв А или а;
	б) в которых имеется ровно 5 букв и.
*/

import java.io.*;
import java.util.Scanner;
 
public class task15_19 {
    public static void main(String[] args) 
		throws IOException
	{  
		try {
			String temp;
			Scanner in = new Scanner(new File("task15_19.txt"));
			Scanner stdin = new Scanner(System.in);
			int o = 0;
			do {
			System.out.print("Eneter option (1-2) > ");
			o = stdin.nextInt();
			} while (o != 1 & o != 2);
			switch (o) {
				case 1:
					int count1 = 0;
					while (in.hasNextLine()) {
						temp = in.nextLine();
						if (temp.startsWith("A") | temp.startsWith("a"))
							count1++;
					}
					System.out.println("Amount of strings starts with 'a' and 'A' - "+count1);
					break;
				case 2:
					int count2 = 0;
					while (in.hasNextLine()) {
						temp = in.nextLine();
						int c = 0;
						for (int i=0; i<temp.length(); i++)
							if (temp.charAt(i) == 'i')
								c++;
						if (c == 5)
							count2++;
					}
					System.out.println("Amount of strings with 5 'i' - "+count2);
					break;
			}
		}
		catch (Exception e) { System.out.println(e); }
	}
}