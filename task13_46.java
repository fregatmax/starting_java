/*
	известно количество очков, набранных каждой из 19 команд-участниц первенства
	по футболу. Перечень очков и команд дан в порядке занятых ими
	мест, т. е. в порядке убывания количества набранных очков (ни одна пара
	команд-участниц не набрала одинаковое количество очков). Выяснилось, что
	в перечень забыли включить еще одну, двадцатую, команду.
	а) Определить, какое место заняла эта команда (количество набранных ею
	очков известно; известно также, что она не стала чемпионом и не заняла последнее место).
	б) Вывести названия команд, набравших меньше очков, чем эта команда.
*/

class Team13_46 {
	int score;
	String name;
	
	Team13_46() {
		java.util.Scanner in = new java.util.Scanner(System.in);
		System.out.println("----------------");
		System.out.print("Enter team's name > ");		
		this.name = in.next();
		String temp = in.nextLine();
		System.out.print("Enter team's score > ");		
		this.score = in.nextInt();
	}
	public void print() {
		System.out.println(name+" with total score:"+score);
	}
}
class task13_46 {
	public static void main(String[] args) {
		final int n = 5; // amount of teams
		Team13_46[] st = new Team13_46[n];
		for (int i=0; i<n; i++)
			st[i] = new Team13_46();
		System.out.println("------------------");
		System.out.println("Forgotten team");
		Team13_46 team = new Team13_46();
		int count = 0;
		while (st[count].score > team.score)
			count++;
		System.out.println(team.name+" took "+(count+1)+" place with total score: "+team.score);
		System.out.println("------------------");
		for (;count<n; count++)
			st[count].print();
	}
}