/*
	Даны два предложения. Напечатать слова, которые 
	встречаются в двух предложениях только один раз.
*/
import java.util.Scanner;

class task9_183{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter 1st sentence > ");
	String s1[] = stdin.nextLine().split(" ");
	System.out.print("Enter 2nd sentence > ");
	String s2[] = stdin.nextLine().split(" ");
	for (int i=0; i<s1.length; i++) {
		int f = 0;
		for (int j=0; j<s2.length; j++)
			if (s1[i].equals(s2[j])) 
				f++;
		for (int j=0; j<s1.length; j++)
			if (s1[i].equals(s1[j]))
				f++;
		if (f == 2)
			System.out.println(s1[i]);
	}
}
}