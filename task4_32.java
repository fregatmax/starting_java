import java.util.Scanner;
class task4_32 {
public static void main(String[] args) {
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter number (4 digits) and a separeted by space > ");
	int x = stdin.nextInt(), a = stdin.nextInt();
	if(x%10+x%100/10==x%1000/100+x/1000)
		System.out.println("Digits 1+2 and 3+4 are equal");
	if((x%10+x%100/10+x%1000/100+x/1000)%3==0)	
		System.out.println("Sum of digits is divisible by 3");
	if(((x%10)*(x%100/10)*(x%1000/100)*(x/1000))%4==0)	
		System.out.println("Composition of digits is divisible by 4");
	if(((x%10)*(x%100/10)*(x%1000/100)*(x/1000))%a==0)	
		System.out.println("Composition of digits is divisible by "+a);
}
}