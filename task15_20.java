/*
	имеется текстовый файл.
	а) Найти длину самой длинной строки.
	б) Найти номер самой длинной строки. Если таких строк несколько, то найти
	номер одной из них.
	в) Напечатать самую длинную строку. Если таких строк несколько, то напе-
	чатать первую из них.
*/

import java.io.*;
import java.util.Scanner;
 
public class task15_20 {
    public static void main(String[] args) 
		throws IOException
	{  
		try {
			String temp;
			Scanner in = new Scanner(new File("task15_20.txt"));
			Scanner stdin = new Scanner(System.in);
			int o = 0;
			do {
			System.out.print("Eneter option (1-3) > ");
			o = stdin.nextInt();
			} while (o < 1 & o > 3);
			switch (o) {
				case 1:
					int maxlen1 = 0;
					while (in.hasNextLine()) {
						temp = in.nextLine();
						maxlen1 = temp.length() > maxlen1? temp.length() : maxlen1;
					}
					System.out.println("Length of the longest string - "+maxlen1);
					break;
				case 2:
					int maxlen2 = 0, count2 = 1;
					for(int i=1; in.hasNextLine(); i++) {
						temp = in.nextLine();
						if (temp.length() > maxlen2) {
							maxlen2 = temp.length();
							count2 = i;
						}
					}
					System.out.println("Number of the longest string - "+count2);
					break;
				case 3:
					int maxlen3 = 0, count3 = 1;
					for(int i=1; in.hasNextLine(); i++) {
						temp = in.nextLine();
						if (temp.length() > maxlen3) {
							maxlen3 = temp.length();
							count3 = i;
						}
					}
					in = new Scanner(new File("task15_20.txt"));
					for(int i=1; i<count3; i++)
						in.nextLine();
					System.out.println("Longest string - "+in.nextLine());
					break;
			}
		}
		catch (Exception e) { System.out.println(e); }
	}
}