/*
	Заполнить массив:
	a) двадцатью первыми натуральными числами, делящимися нацело на 13
	или на 17 и находящимися в интервале, левая граница которого равна 300;
	
	б) тридцатью первыми простыми числами (простым называется натураль-
	ное число, большее 1, не имеющее других делителей, кроме единицы и са-
	мого себя)
*/

class task11_12 {
	public static void main(String[] args) {
		int ar[] = new int[20];
		System.out.println("20 first numbers divisible by 13 and 17");
		for (int i=0, j=1; i<20; i++, j++) {
			for (; j<301; j++)
				if(j%13 == 0 | j%17 == 0)
					break;
			ar[i] = j;
			System.out.print(ar[i]+" ");
		}
		System.out.println("\n----------------------------------------");
		System.out.println("30 first prime numbers");
		ar = new int[30];
		for (int i=0, j=2; i<30; i++, j++) {
			for (;; j++)
				if(isPrime(j))
					break;
			ar[i] = j;
			System.out.print(ar[i]+" ");
		}
	}
	static boolean isPrime(int n) {
		if (2 > n/2) 
			return true;
		return n%2 != 0 ? isPrime(n, 3) : false;
	}
	static boolean isPrime(int n, int del) {
		if (del > n/2) 
			return true;
		return n%del != 0 ? isPrime(n, ++del) : false;
	}
}