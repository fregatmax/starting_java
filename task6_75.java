// Дана последовательность из 20 чисел из интервала от 0 до 66, представляю-
// щих собой условные обозначения костей домино (например, число 42 есть
// обозначение кости домино "4–2" или "2–4", число 33 — кости "3–3" и т. п.).
// Определить, соответствует ли последовательность чисел ряду костей домино,
// выложенному по правилам этой игры. Рассмотреть два случая:
// а) последняя цифра каждого числа соответствует количеству точек на правой
// половине кости домино;
// б) количеству точек на правой и левой половинах кости домино может соот-
// ветствовать любая из цифр заданных чисел. 
import java.util.Scanner;

class task6_75{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter n > ");
	int n = stdin.nextInt(), a=n;
	int count = 1, k1=0, k2=0, x=3; 			// 1st for a) and 2nd for b) x=3 -'11'
	do	{										// x=1 if previous not turned x=2 if turned
		System.out.print("Enter n > ");
		n = stdin.nextInt();
		if(n/10!=a%10) k1=1;
		switch(x) {
			case 1:
				if(n/10!=a%10)
					if(n%10!=a%10)	k2=1;
					else x=2;
				else x=1;
				break;
			case 2:
				if(n/10!=a/10)
					if(n%10!=a/10) k2=1;
					else x=2;
				else x=1;
				break;
			case 3:
				if((n/10!=a%10)&&(n/10!=a/10))
					if((n%10!=a%10)&&(n%10!=a/10)) k2=1;
					else x=2;
				else x=1;
				break;
		}
		if(n%10==n/10)	x=3;
		a = n;
		count++;
//		System.out.println("k1 > "+k1+" k2 > "+k2+" x > "+x);
	} while(count!=20);
	if(k1==0)	System.out.println("Corresponds to rules	(a)");
	else		System.out.println("Doesn't correspond to rules	(a)");
	if(k2==0)	System.out.println("Corresponds to rules	(b)");
	else		System.out.println("Doesn't correspond to rules	(b)");

}
}