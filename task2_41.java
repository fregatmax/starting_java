import java.util.Scanner;

class task2_41 {
public static void main(String[] args) {
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter y (0<=y<2*Pi) > ");
	double y = stdin.nextDouble(),md;
	byte h,m;
	h = (byte)(y/(Math.PI/6));
	m = (byte)(y%(Math.PI/6)*120);
	md = m*Math.PI/30;
	System.out.print(h+":"+m+"\t Minute pointer radians > "+md);	
}
}