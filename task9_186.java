/*
	Даны два предложения. Напечатать слова, которые 
	встречаются в двух предложениях только один раз.
*/
import java.util.Scanner;

class task9_186{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter 1st sentence > ");
	String s[] = stdin.nextLine().split(" ");
	String sf = new String();
	int length = 0;
	for (int i=0; i<s.length; i++) 
		length += s[i].length();
	System.out.println("Length without spaces - "+length);
	System.out.print("Enter required length > ");
	int l = stdin.nextInt();
	for (int i=0; i<l; i++)
		System.out.print("-");
	length = l - length;
	System.out.println();
	if (length < 0) {
		System.out.println("Can't do this");
		return;
	}
	else do
		for (int j=0; j<s.length-1; j++, length--)
			if (length == 0)
				break;
			else 
				s[j] = s[j] + " ";
		while(length > 0);
	for (int i=0; i<s.length; i++) 
		sf = sf + s[i];
	System.out.println(sf);
}
}