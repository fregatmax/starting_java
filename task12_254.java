/*
	Дан двумерный массив из двадцати строк и трех столбцов. Перенести пер-
	вые k строк в конец массива, соблюдая порядок их следования.
*/

import java.util.Scanner;
import java.util.Arrays;

class task12_254 {
	public static int[][] rndMatrix(int n, int m) {
		java.util.Random rn = new java.util.Random();
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++)  {
				ar[i][j] = rn.nextInt(20);	
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		return ar;
	}
	public static void printMatrix(int[][] ar) {
		for (int i=0; i<ar.length; i++) {
			for (int j=0; j<ar[i].length; j++) 
				System.out.printf("%3d ",ar[i][j]);
			System.out.println();
		}
	}
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		final int n = 3, m = 20;
		int ar[][] = rndMatrix(n,m);
		System.out.print("Enter k > ");
		int k = in.nextInt();
		for (int l=k; l>0; l--) {
			for (int i=1; i<m; i++)
				for (int j=0; j<n; j++)
					ar[i][j] = ar[i-1][j] + ar[i][j] - (ar[i-1][j] = ar[i][j]);		
		}
		System.out.println("-----------------");
		printMatrix(ar);
	}
}