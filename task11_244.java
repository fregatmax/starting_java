/*
	Дан массив. Переписать его положительные элементы во второй массив,
	а остальные — в третий. Во втором и третьем массивах значения элементов
	первого массива должны быть записаны:
	а) на тех же местах, что и в исходном массиве;
	б) подряд с начала массива.
*/

import java.util.Scanner;
import java.util.Arrays;

class task11_244 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter length of the array >");
		int len = in.nextInt();
		int ar[] = new int[len];
		System.out.print("Enter array elements > ");
		for (int i=0; i<len; i++) {
			ar[i] = in.nextInt();
		}
		System.out.print("Enter option (1..2) >");
		int o = in.nextInt();
		switch (o) {
			case 2: {
				int[] ar1 = new int[ar.length],
						ar2 = new int[ar.length];
				int count = 0;
				for (int i=0; i<ar.length; i++)
					if (ar[i] > 0)
						ar1[count++] = ar[i];
					else
						ar2[i-count] = ar[i];
				ar1 = Arrays.copyOf(ar1,count);
				ar2 = Arrays.copyOf(ar2,ar.length-count);
				System.out.print("1st: ");
				for (int i=0; i<len; i++) 
					System.out.print(ar[i]+" ");
				System.out.print("\n2nd: ");
				for (int i=0; i<ar1.length; i++) 
					System.out.print(ar1[i]+" ");
				System.out.print("\n3rd: ");
				for (int i=0; i<ar2.length; i++) 
					System.out.print(ar2[i]+" ");
				}
				break;
			case 1: {
				int[] ar1 = new int[ar.length],
						ar2 = new int[ar.length];
				for (int i=0; i<ar.length; i++)
					if (ar[i] > 0)
						ar1[i] = ar[i];
					else
						ar2[i] = ar[i];
				System.out.print("1st: ");
				for (int i=0; i<len; i++) 
					System.out.print(ar[i]+" ");
				System.out.print("\n2nd: ");
				for (int i=0; i<len; i++) 
					System.out.print(ar1[i]+" ");
				System.out.print("\n3rd: ");
				for (int i=0; i<len; i++) 
					System.out.print(ar2[i]+" ");
				}
				break;
		}
	}
}