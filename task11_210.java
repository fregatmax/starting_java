/*
	имеется список учащихся класса с указанием роста каждого из них.
	Определить, перечислены ли ученики в списке в порядке убывания их роста.
*/

import java.util.Scanner;

class task11_210 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter length of the array >");
		int len = in.nextInt();
		int ar[] = new int[len];
		for (int i=0; i<ar.length; i++) {
			ar[i] = (int)(Math.random()*10+160-i*2);
			System.out.print(ar[i]+" ");
		}
		for (int i=1; i<ar.length; i++)
			if (ar[i-1] < ar[i]) {
				System.out.println("\nNot descending from "+(i+1));
				return;
			}
		System.out.println("\nDescending");
	}
}