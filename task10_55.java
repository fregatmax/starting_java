/*
	Написать рекурсивную процедуру перевода натурального 
	числа из десятичной системы счисления в N-ричную.
	Значение N в основной программе вводится
	с клавиатуры (2 N 16).
*/
import java.util.Scanner;

class task10_55{
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		System.out.print("Enter n > ");
		int n = in.nextInt();
		System.out.print("Enter numeral system (2..16) > ");
		int N = in.nextInt();
		decToN(n,N);
	}
	static void decToN(int n, int N) {
		if (n/N == 0) {
			System.out.print(n%N);
			return;
		}
		decToN(n/N,N);
		System.out.print(n%N);
		return;
	}
}