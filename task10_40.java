/*
	Даны основания и высоты двух равнобедренных трапеций. Найти сумму их
	периметров и сумму их площадей. (Определить процедуру для расчета 
	периметра и площади равнобедренной трапеции по ее основаниям и высоте.)
*/
import java.util.Scanner;

class task10_40{
	public static void main(String[] args){
		Scanner stdin = new Scanner(System.in);
		for (int i=1; i<3; i++) {
			System.out.print("Enter a,b h for trapeze number "+i+" > ");
			int a = stdin.nextInt(), b = stdin.nextInt(), h = stdin.nextInt();
			System.out.println("Perimeter = "+perimeter(a,b,h)+" square = "+square(a,b,h));
		}
	}
	static double perimeter(int a, int b, int h) { //P = a+b+2*√(h²+(a-b)²/4)
		return a + b + 2 * Math.sqrt(h * h +
									((a - b) * (a - b))/4.0);
	}
	static double square(int a, int b, int h) {
		return (a + b) / 2.0 * h;
	}
}