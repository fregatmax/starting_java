import java.util.Scanner;

class task7_91{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter capacity of the car > ");
	int c = stdin.nextInt(), cargo = 0, w;
	do {
		System.out.print("Enter weight of cargo (0 - exit) > ");
		w = stdin.nextInt();
		cargo += w;
	} while(w != 0);
	if(cargo > c)	System.out.println("Cargo exceeded capacity");
	else 			System.out.println("Cargo didn't exceed capacity");
}
}