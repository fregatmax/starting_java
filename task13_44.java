/*
	Количество мячей, забитых и пропущенных футбольной командой в каждой
	из 22 игр, записано в массиве, элементами которого являются величины типа
	"запись".
	а) Для каждой проведенной игры напечатать словесный результат: "выигрыш", "ничья" или "проигрыш".
	б) Определить количество выигрышей данной команды.
	в) Определить количество выигрышей и количество проигрышей данной команды.
	г) Определить количество выигрышей, количество ничьих и количество проигрышей данной команды.
	д) Определить общее число очков, набранных командой (за выигрыш дается 3 очка,
	за ничью — 1, за проигрыш — 0).
*/

class Team13_44 {
	int scored;
	int missed;
	
	Team13_44() {
		java.util.Scanner in = new java.util.Scanner(System.in);
		System.out.println("----------------");
		System.out.print("Enter team's scored and missed balls separated by space > ");		
		this.scored = in.nextInt();
		this.missed = in.nextInt();
	}
}
class task13_44 {
	public static void main(String[] args) {
		final int n = 5; // amount of games
		Team13_44[] ar = new Team13_44[n];
		for (int i=0; i<n; i++)
			ar[i] = new Team13_44();
		System.out.println("------------------");
		int win = 0, loss = 0, draw = 0;
		for (int i=0; i<n; i++)
			if (ar[i].scored == ar[i].missed) {
				System.out.println((i+1)+"\tgame - draw");
				draw++;
			}
			else if (ar[i].scored > ar[i].missed) {
				System.out.println((i+1)+"\tgame - win");
				win++;
			}
			else {
				System.out.println((i+1)+"\tgame - loss");
				loss++;
			}
		System.out.println("Wins - "+win+" Losses - "+loss+" Draws - "+draw);
		System.out.println("Total score - "+(win*3+draw));
	}
}