/*
	имеется текстовый файл, в каждой строке которого первые два символа яв-
	ляются буквами. Получить:
	а) слово, образованное первыми буквами каждой строки;
	б) слово, образованное вторыми буквами каждой строки;
*/

import java.io.*;
import java.util.Scanner;
 
public class task15_23 {
    public static void main(String[] args) 
		throws IOException
	{  
		try {
			String temp;
			Scanner in = new Scanner(new File("task15_23.txt"));
			Scanner stdin = new Scanner(System.in);
			int o = 0;
			do {
			System.out.print("Eneter option (1-2) > ");
			o = stdin.nextInt();
			} while (o < 1 & o > 2);
			switch (o) {
				case 1:
					while (in.hasNextLine())
						System.out.print(in.nextLine().charAt(0));
					break;
				case 2:
					while (in.hasNextLine())
						System.out.print(in.nextLine().charAt(1));
					break;
			}
		}
		catch (Exception e) { System.out.println(e); }
	}
}