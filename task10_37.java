/*
	Составить процедуру, "рисующую" на экране прямоугольник из симво-
	лов "*".
*/
import java.util.Scanner;

class task10_37{
	public static void main(String[] args){
		Scanner stdin = new Scanner(System.in);
		System.out.print("Enter a(height) and b(width) > ");
		int a = stdin.nextInt(), b = stdin.nextInt();
		System.out.println(a+" "+b);
		drawRectangle(a,b);
	}
	static void drawRectangle(int a, int b) {
		System.out.println();
		for (int i=0; i<b; i++) 
			System.out.print("*");
		for (int i=1; i<a-1; i++) {
			System.out.print("\n*");
			for (int j=1; j<b-1; j++)
				System.out.print(" ");
			System.out.print("*");
		}
		System.out.println();
		for (int i=0; i<b; i++) 
			System.out.print("*");
		System.out.println();
	}
}