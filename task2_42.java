import java.util.Scanner;

class task2_42 {
public static void main(String[] args) {
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter h(1..12), m(0..59) separated by a space > ");
	byte h = stdin.nextByte(), m = stdin.nextByte(),m2;
	double H = 30*h*Math.signum(12-h)+0.5*m, M = 6*m;
	m2 = (byte)((1-Math.signum(H-M))*((360-M)/6+((h+1)*30)/5.5)/2+(1+Math.signum(H-M))*(H-M)/5.5);
	System.out.print(m2+" minutes until the pointers will be parallel");
}
}