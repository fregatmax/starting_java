/*
	имеется текстовый файл, содержащий 20 строк. Переписать каждую из его
	строк в массив в том же порядке.
*/

import java.io.*;
import java.util.Scanner;
 
public class task15_17 {
    public static void main(String[] args) 
		throws IOException
	{  
		String ar[] = new String[20];
		try{
			Scanner in = new Scanner(new File("task15_17.txt"));
			for(int i=0; in.hasNextLine() & i<20; i++)
				ar[i] = in.nextLine();
			for(int i=0; i<20; i++)
				System.out.println((i+1)+" - \t"+ar[i]);
		}
		catch(Exception e){System.out.println(e);}
    }
}