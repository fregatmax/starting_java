/*
	В конец существующего текстового файла записать три новые строки текста.
	Записываемые строки вводятся с клавиатуры.
*/


import java.io.*;
import java.util.Scanner;
 
public class task15_6 {
    public static void main(String[] args) 
		throws IOException
	{  
		Scanner in = new Scanner(System.in);
		try{
			FileWriter fw = new FileWriter("task15_6.txt",true);
			System.out.print("Enter 3 sentences (end with enter) > ");
			for (int i=0; i<3; i++)
				fw.write("\n"+in.nextLine());
			fw.close();
		}
		catch(Exception e){System.out.println(e);}
    } 
}