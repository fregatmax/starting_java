import java.util.Scanner;

class task6_84{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter n > ");
	int n = stdin.nextInt(), max, min, order = 0; 
	max = min = n%10;		// order = 1 min to the left of max
	while(n>=10)	{		// order = 2 max to the left of min
		n /= 10;
		if(max<=n%10) {
			max = n%10;
			order = 2;
		}
		if(min>=n%10) {
			min = n%10;
			order = 1;
		}
	}
	if(order==1)	
		System.out.println("Min to left of max");
	else 
		System.out.println("Max to left of min");
}
}