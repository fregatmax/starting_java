/*
	Даны два массива одного размера, в которых нет нулевых элементов. 
	Получить третий массив, каждый элемент которого равен 1, если 
	элементы заданных массивов с тем же номером имеют одинаковый знак,
	и равен нулю в противном случае.
*/

import java.util.Scanner;

class task11_247 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter length of the array >");
		int len = in.nextInt();
		int ar1[] = new int[len], ar2[] = new int[len];
		System.out.print("Enter 1st array elements > ");
		for (int i=0; i<len; i++) 
			ar1[i] = in.nextInt();
		System.out.print("Enter 2nd array elements > ");
		for (int i=0; i<len; i++) 
			ar2[i] = in.nextInt();
		int[] ar3 = new int[ar1.length];
		for (int i=0; i<ar1.length; i++)
			ar3[i] = ar1[i] * ar2[i] > 0 ? 1 : 0;
		System.out.print("1st: ");
		for (int i=0; i<len; i++) 
			System.out.print(ar1[i]+" ");
		System.out.print("\n2nd: ");
		for (int i=0; i<ar1.length; i++) 
			System.out.print(ar2[i]+" ");
		System.out.print("\n3rd: ");
		for (int i=0; i<ar2.length; i++) 
			System.out.print(ar3[i]+" ");
	}
}
