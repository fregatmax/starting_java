/*
	имеется текстовый файл. Выяснить, имеется ли в нем строка, начинающаяся
	с буквы Т. Если да, то определить номер первой из таких строк.
*/

import java.io.*;
import java.util.Scanner;
 
public class task15_21 {
    public static void main(String[] args) 
		throws IOException
	{  
		try{
			String temp;
			Scanner in = new Scanner(new File("task15_21.txt"));
			boolean fl = false;
			for(int i=1; in.hasNextLine(); i++)
				if (in.nextLine().startsWith("T")) {
					System.out.println("Nubmer of string that starts with 'T' - "+i);
					fl = true;
					break;
				}
			if (!fl)
				System.out.println("There no such string that starts with 'T'");
		}
		catch(Exception e){System.out.println(e);}
    }
}