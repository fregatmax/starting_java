/*
	имеется текстовый файл. Напечатать:
	а) первый символ первой строки;
	б) пятый символ первой строки;
	в) первые 10 символов первой строки;
	г) символы с s1-го по s2-й в первой строке;
	д) первый символ второй строки;
	е) k-й символ n-й строки.
*/

import java.io.*;
import java.util.Scanner;
 
public class task15_22 {
    public static void main(String[] args) 
		throws IOException
	{  
		try {
			String temp;
			Scanner in = new Scanner(new File("task15_22.txt"));
			Scanner stdin = new Scanner(System.in);
			int o = 0;
			do {
			System.out.print("Eneter option (1-6) > ");
			o = stdin.nextInt();
			} while (o < 1 & o > 6);
			switch (o) {
				case 1:
					if (in.hasNext())
						System.out.println(in.nextLine().charAt(0));
					break;
				case 2:
					if (in.hasNext())
						System.out.println(in.nextLine().charAt(4));
					break;
				case 3:
					temp = in.nextLine();
					for(int i=0; i<10; i++)
						System.out.print(temp.charAt(i));
					System.out.println();
					break;
				case 4:	
					int s1 = 0, s2 = 0;
					do {
						System.out.print("Eneter s1 and s2 (s1<s2) > ");
						s1 = stdin.nextInt();
						s2 = stdin.nextInt();
					} while (s1 > s2);
					temp = in.nextLine();
					for(int i=s1-1; i<(temp.length()-1) & i<s2; i++)
						System.out.print(temp.charAt(i));
					break;
				case 5:
					in.nextLine();
					if (in.hasNext())
						System.out.println(in.nextLine().charAt(0));
					break;
				case 6:
					int k = 0, n = 0;
					System.out.print("Eneter k(number in string) and n(number of string > ");
					k = stdin.nextInt();
					n = stdin.nextInt();
					for(int i=1; in.hasNextLine() & i<n; i++)
						in.nextLine();
					try {
						System.out.println(in.nextLine().charAt(k));
					}
					catch (IndexOutOfBoundsException ex) {
						System.out.println("Out of line borders");
					}
					break;
			}
		}
		catch (Exception e) { System.out.println(e); }
	}
}