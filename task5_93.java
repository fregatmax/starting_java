import java.util.Scanner;
class task5_93{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter n > ");
	int n = stdin.nextInt();
	double s=0,c=0,total=0;
	for(int i=1; i<=n; i++){
		s += Math.sin(i);
		c += Math.cos(i);
		total += c/s;
	}
	System.out.println(total);
}
}