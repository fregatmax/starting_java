import java.util.Scanner;

class task6_83{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter n,a > ");
	int n = stdin.nextInt(), a = stdin.nextInt(), max, min;
	max = min = n%10;
	while(n>=10)	{
		n /= 10;
		if(max<n%10)	max = n%10;
		if(min>n%10)	min = n%10;
	}
	if((max+min)%a==0)	
		System.out.println("Sum of max and min digits of \"n\" is divisible by "+a);
	else 
		System.out.println("Sum of max and min digits of \"n\" is not divisible by "+a);
}
}