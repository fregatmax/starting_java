/*
	Дан двумерный массив. Вывести на экран его элементы следующим образом:
	а) сначала элементы первой строки справа налево, затем второй строки
	справа налево и т. п.;
	б) сначала элементы первой строки справа налево, затем второй строки слева
	направо и т. п.;
	в) сначала элементы первого столбца сверху вниз, затем второго столбца
	сверху вниз и т. п.;
	г) сначала элементы первого столбца снизу вверх, затем второго столбца
	снизу вверх и т. п.
*/
import java.util.Scanner;

class task12_34 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter n(width) and m(height) of 2D-array separated by space > ");
		final int n = in.nextInt(), m = in.nextInt();
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++) {
				ar[i][j] = (int)(Math.random()*10);	
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		System.out.println("\n----------------------------------------");
		int o = 0;
		do {
			System.out.print("\nEnter option (1..4), 0 - exit > ");
			o = in.nextInt();
			switch (o) {
				case 1:
					for (int i=0; i<m; i++) {
						for (int j=n-1; j>=0; j--)
							System.out.printf("%2d ",ar[i][j]);
						System.out.println();
					}		
					break;
				case 2:
					for (int i=0; i<m; i++) {
						if (i%2 == 0)
							for (int j=n-1; j>=0; j--)
								System.out.printf("%2d ",ar[i][j]);
						else
							for (int j=0; j<n; j++)
								System.out.printf("%2d ",ar[i][j]);
						System.out.println();
					}		
					break;
				case 3:
					for (int j=0; j<n; j++) {
						for (int i=0; i<m; i++)
							System.out.printf("%2d ",ar[i][j]);
						System.out.println();
					}		
					break;
				case 4:
					for (int j=0; j<n; j++) {
						for (int i=m-1; i>=0; i--)
							System.out.printf("%2d ",ar[i][j]);
						System.out.println();
					}		
					break;
			}
		} while (o != 0);
	}
}
