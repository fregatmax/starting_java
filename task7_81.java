import java.util.Scanner;

class task7_81{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter number n > ");
	int n = stdin.nextInt(), x, x1, max, order;
	System.out.print("Enter x(1) > ");
	x = stdin.nextInt();
	max = 0;
	order = 0;
	for(int i=2;i<=n;i++) {
		x1 = x;
		System.out.print("Enter x("+i+") > ");
		x = stdin.nextInt();
		if(x+x1 > max)	{
			max = x+x1;
			order = i;
		}
	}
	System.out.println("Max sum of two adjacent numbers: "+max);
	System.out.println("Their order numbers are: "+(order-1)+" and "+order);
}
}