import java.util.Scanner;
class task4_17 {
public static void main(String[] args) {
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter circle and triangle areas separated by space > ");
	double c = stdin.nextDouble(), t = stdin.nextDouble();
	//	S = PI*R^2
	//	S = sqrt(3)/4*a^2 = sqrt(27)*r^2  R = a/sqrt(3)
	if((t/Math.sqrt(27))>=(c/Math.PI))	
	System.out.println("Circle fits in triangle");
	
	if((4*t/Math.sqrt(27))<=(c/Math.PI))	
	System.out.println("Triangle fits in circle");
}
}