/*
	Даны два предложения. В каком из них доля (в %) буквы б больше. 
	Определить функцию для расчета доли некоторой буквы в предложении.
*/
import java.util.Scanner;

class task10_30{
	public static void main(String[] args){
		Scanner stdin = new Scanner(System.in);
		String s[] = new String[2];
		for (int i=0; i<2; i++) {
			System.out.print("Enter sentence > ");
			s[i] = stdin.nextLine();
		}
		if (weightOfB(s[0]) > weightOfB(s[1]))
			System.out.print("The proportion of \"b\" in first sentence is bigger");
		else 
			System.out.print("The proportion of \"b\" in second sentence is bigger");
	}
	static int weightOfB(String s) {
		double r = 0;
		for (int i=0; i<s.length(); i++)
			if (s.charAt(i) == 'b')
				r++;
		return (int)(r/s.length()*100);
	}
}