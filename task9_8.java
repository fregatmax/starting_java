import java.util.Scanner;

class task9_8{
public static void main(String[] args) {
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter name of the city > ");
	String town = stdin.nextLine();
	int even = town.length();
	if(even%2 == 0)	System.out.println("Even number of characters");
	else 			System.out.println("Odd number of characters");
}
}