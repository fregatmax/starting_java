/*
	используя датчик случайных чисел, заполнить
	массив из двадцати элементов неповторяющимися числами
*/

class task11_11{
	public static void main(String[] args) {	
		int ar[] = new int[20];
		for (int i=0; i<20; i++) {
			boolean f = true;
			do {
				ar[i] = (int)(Math.random()*1000);
				for (int j=i-1; j>=0; j--)
					if (ar[i] == ar[j]) {
						f = false;
						break;
					}
			} while (!f);
			System.out.print(ar[i]+" ");
		}
	}
}