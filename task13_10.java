/*
	известны фамилии 25 человек, их семейное положение: женат (замужем)
	или нет, и сведения о наличии детей (есть или нет). Определить фамилии
	женатых (замужних) людей, имеющих детей.
*/

class Human13_10 {
	String lastname;
	char status;
	char children;
	
	Human13_10() {
		java.util.Scanner in = new java.util.Scanner(System.in);
		System.out.println("----------------");
		System.out.print("Enter lastname > ");		
		this.lastname = in.next();
		String temp = in.nextLine();
		System.out.print("Marital status m(married)/s(single) > ");		
		this.status = in.nextLine().charAt(0);
		System.out.print("Has child(ren) ? (y/n) >");
		this.children = in.nextLine().charAt(0);
		//temp = in.nextLine();
	}
	public void print() {
		System.out.println(lastname);
	}
}
class task13_10 {
	public static void main(String[] args) {
		final int n = 25; // amount of customers
		Human13_10[] hum = new Human13_10[n];
		for (int i=0; i<n; i++)
			hum[i] = new Human13_10();
		System.out.println("----------------------------");
		System.out.println("Married people with children");
		for (int i=0; i<n; i++)
			if (hum[i].status == 'm' && hum[i].children == 'y')
				hum[i].print();
	}
}