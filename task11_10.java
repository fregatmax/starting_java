/*
	Дано натуральное число n (n <= 999999). Заполнить массив его цифрами,
	расположенными в обратном порядке (первый элемент равен последней
	цифре, второй — предпоследней и т. д.). Незаполненные элементы массива
	должны быть равны нулю. Элементы массива, являющиеся цифрами
	числа n, вывести на экран.
*/

import java.util.Scanner;

class task11_10{
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		System.out.print("Enter n (<1000000 > ");
		int n = in.nextInt(), ar[] = new int[6];
		for (int i=1, j=0; j<6; i*=10, j++)
			ar[j] = n/i%10;
		int count = 5;
		while (ar[count] == 0)
			count--;
		for (int i=count; i>=0; i--)
			System.out.print(ar[i]);
	}
}