/*
	Дан массив целых чисел.
	а) Все элементы, кратные числу 10, заменить нулем.
	б) Все нечетные элементы удвоить, а четные уменьшить вдвое.
	в) Нечетные элементы уменьшить на m, а элементы с нечетными номерами
	увеличить на n.
*/
import java.util.*;

class task11_53 {
	public static void main(String[] args) {
		int ar[] = new int[20];
		System.out.println("Precipitation at each day in June");
		for (int i=0; i<ar.length; i++) {
			ar[i] = (int)(Math.random()*100);
			System.out.print(ar[i]+" ");
		}
		System.out.println("\n----------------------------------------");
		Scanner in = new Scanner(System.in);
		int o = 0;
		do {
			System.out.print("\nEnter option (1..3), 0 - exit > ");
			o = in.nextInt();
			int arr[] = Arrays.copyOf(ar,ar.length);
			switch (o) {
				case 1:
					for (int i=0; i<arr.length; i++) {
						if (arr[i]%10 == 0)
							arr[i] = 0;
						System.out.print(arr[i]+" ");
					}
					break;
				case 2:
					for (int i=0; i<arr.length; i++) {
						if (arr[i]%2 == 1)
							arr[i] *= 2;
						else
							arr[i] /= 2;
						System.out.print(arr[i]+" ");
					}
					break;
				case 3:
					System.out.print("Enter option n and m > ");
					int n = in.nextInt(), m = in.nextInt();
					for (int i=0; i<arr.length; i++) {
						if (arr[i]%2 == 1)
							arr[i] -= m;
						if ((i+1)%2 == 1)
							arr[i] += n;
						System.out.print(arr[i]+" ");
					}
					break;
			}
		} while (o != 0);
	}
}
