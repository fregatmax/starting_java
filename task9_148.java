/*
	Дан текст. Найти наибольшее количество идущих подряд цифр.
*/

import java.util.Scanner;

class task9_148{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter text > ");
	String e = stdin.nextLine();
	int max = 0;
	for (int i=0; i<e.length(); i++) {
		int t = 0;
		while (i < e.length() && e.charAt(i) > 47 && e.charAt(i) < 58) {
			t++;
			i++;
		}
		if (t > max)
			max = t;
	}
	System.out.println(max);	
}
}