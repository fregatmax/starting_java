/*
	Дано предложение. Записать каждый из его символов в файл. Какая 
	структура файла наиболее целесообразна для решения этой задачи?
*/

import java.io.*;
import java.util.Scanner;
 
public class task14_6 {
    public static void main(String[] args) 
		throws IOException
	{  
		Scanner in = new Scanner(System.in);
		System.out.print("Enter sentence for file > ");
        String text = in.nextLine();
		FileOutputStream fout;
		try {
			fout = new FileOutputStream("task14_6.txt");
		} catch(FileNotFoundException e) {
			System.out.println("Error Opening Output File");
			return;
		}
		try {
			byte[] buffer = text.getBytes();
				fout.write(buffer);
		}
        catch(IOException ex){    
            System.out.println(ex.getMessage());
        }
		fout.close();
    } 
}