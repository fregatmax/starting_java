/*
	известны данные о 30 учениках: фамилия, класс и оценка по информатике.
	Определить фамилии учеников 9-х классов, имеющих оценку "5".
*/


class Student13_11 {
	String lastname;
	int grade;
	int assessment;
	
	Student13_11() {
		java.util.Scanner in = new java.util.Scanner(System.in);
		System.out.println("----------------");
		System.out.print("Enter student's lastname > ");		
		this.lastname = in.next();
		String temp = in.nextLine();
		System.out.print("Enter student's grade > ");		
		this.grade = in.nextInt();
		System.out.print("Enter student's assessment in computer scince > ");
		this.assessment = in.nextInt();
	}
	public void print() {
		System.out.println(lastname);
	}
}
class task13_11 {
	public static void main(String[] args) {
		final int n = 30; // amount of students
		Student13_11[] st = new Student13_11[n];
		for (int i=0; i<n; i++)
			st[i] = new Student13_11();
		System.out.println("---------------");
		System.out.println("Students from 9 grade with assessment '5' in computer scince");
		for (int i=0; i<n; i++)
			if (st[i].assessment == 5 && st[i].grade == 9)
				st[i].print();
	}
}