/*
	Дан одномерный массив m из 28 элементов. 
	Перенести первые k элементов в конец.
*/

import java.util.Scanner;

class task11_178 {
	public static void main(String[] args) {
		int len = 10;
		int ar[] = new int[len];
		for (int i=0; i<ar.length; i++) {
			ar[i] = (int)(Math.random()*(30));
			System.out.print(ar[i]+" ");
		}
		Scanner in = new Scanner(System.in);
		System.out.print("\nEnter k >");
		int k = in.nextInt();
		for (int j=k; j>0; j--)
			for (int i=1; i<ar.length; i++)
				ar[i] = ar[i-1] + ar[i] - (ar[i-1] = ar[i]);
		System.out.println();
		for (int i=0; i<len; i++) 
			System.out.print(ar[i]+" ");
	}
}