/*
	Даны стороны двух треугольников. Найти сумму их
	периметров и сумму их площадей. (Определить 
	процедуру для расчета периметра и площади 
	треугольника по его сторонам.)
*/
import java.util.Scanner;

class task10_39{
	public static void main(String[] args){
		Scanner stdin = new Scanner(System.in);
		for (int i=1; i<3; i++) {
			System.out.print("Enter a,b c for triangle number "+i+" > ");
			int a = stdin.nextInt(), b = stdin.nextInt(), c = stdin.nextInt();
			System.out.println("Perimeter = "+perimeter(a,b,c)+" square = "+square(a,b,c));
		}
	}
	static int perimeter(int a, int b, int c) {
		return a + b + c;
	}
	static double square(int a, int b, int c) {
		int p = perimeter(a,b,c)/2;
		return Math.sqrt(p * (p - a) * (p - b) * (p - c));
	}
}