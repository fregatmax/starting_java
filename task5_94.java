import java.util.Scanner;
class task5_94{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter n (6 digits) > ");
	int n = stdin.nextInt();
	int s=0;
	for(int i=1; i<=6; i++){
		s += n%10;
		n /= 10;
	}
	System.out.println(s);
}
}