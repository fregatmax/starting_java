import java.util.Scanner;

class task8_23{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	int table[][] = new int[5][7];
	int choice;
	for(int i=0;i<5;i++) {
				System.out.print("Enter price of product "+(i+1)+" > ");
				table[i][0] = stdin.nextInt();
				System.out.print("Enter the amount sold at each day(6) > ");
				for(int j=1;j<7;j++)
					table[i][j] = stdin.nextInt();
				}
	do {
	System.out.print("Enter an option (0-6) > ");
	choice = stdin.nextInt();
	int revenue, max, product, day;
	switch(choice) {
		case 1: 
			for(int i=0;i<5;i++) {
				revenue = 0;
				for(int j=1;j<7;j++)
					revenue += table[i][0]*table[i][j];
				System.out.println("\tTotal revenue from product "+(i+1)+"\t = "+revenue);
				}
			break;
		case 2:
			for(int i=1;i<7;i++) {
				revenue = 0;
				for(int j=0;j<5;j++)
					revenue += table[j][0]*table[j][i];
				System.out.println("\tTotal revenue at day "+(i)+"\t = "+revenue);	
			}
			break;
		case 3:
			revenue = 0;
			for(int i=0;i<5;i++)
				for(int j=1;j<7;j++)
					revenue += table[i][0]*table[i][j];
			System.out.println("\tTotal revenue for six days = "+revenue);
			break;
		case 4:
			max = product = 0;
			for(int i=0;i<5;i++){
				revenue = 0;
				for(int j=1;j<7;j++)
					revenue += table[i][0]*table[i][j];
				if(max == 0 | max < revenue) { 
					max = revenue;
					product = i;
				}
			}
			System.out.println("\tProduct number "+(product+1)+" gave maximum revenue - "+max);
			break;
		case 5:
			max = day = 0;
			for(int i=1;i<7;i++) {
				revenue = 0;
				for(int j=0;j<5;j++)
					revenue += table[j][0]*table[j][i];
				if(max == 0 | max < revenue) { 
					max = revenue;
					day = i;
				}
			}
			System.out.println("\tDay number "+(day)+" gave maximum revenue - "+max);
			break;
		case 6:
			System.out.print("Enter a > ");
			int a = stdin.nextInt();
			day = 0;
			for(int i=1;i<7;i++) {
				revenue = 0;
				for(int j=0;j<5;j++)
					revenue += table[j][0]*table[j][i];
				if(a < revenue)
					day++;
			}
			System.out.println("\tNumber of days when revenue was more than "+a+"  - "+day);
			break;
	}
	} while(choice!=0);
}
}