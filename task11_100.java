/*
	Определить, имеются ли в одномерном массиве одинаковые элементы.
*/

import java.util.Scanner;

class task11_100 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter length of the array >");
		int len = in.nextInt();
		int ar[] = new int[len];
		System.out.print("Enter array elements > ");
		for (int i=0; i<len; i++) {
			ar[i] = in.nextInt();
		}
		for (int i=0; i<len; i++) 
			for (int j=i+1; j<len; j++)
				if (ar[i] == ar[j]) {
					System.out.println("Array contains same elements");
					return;
				}
		System.out.println("Array doesn't contain same elements");
	}
}