/*
	В массиве хранится информация о количестве людей, живущих на каждом
	из 15 этажей дома (на первом этаже — в первом элементе массива, 
	на втором — во втором и т. д.). Определить два этажа, на которых проживает
	меньше всего людей.
	
	Задачу решить, не используя два прохода по массиву.
*/

import java.util.Scanner;

class task11_139 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		// System.out.print("Enter length of the array >");
		int len = 15;
		int ar[] = new int[len];
		System.out.print("Enter number of citizens at each floor > ");
		for (int i=0; i<len; i++) {
			ar[i] = in.nextInt();
		}
		int m1 = 0, m2 = 1;
		for (int i=1; i<len; i++) 
			if (ar[i] < ar[m1]) {
				m2 = m1;
				m1 = i;
			}
			else if (ar[i] < ar[m2])
				m2 = i;
		System.out.println((m1+1)+" "+(m2+1));
	}
}