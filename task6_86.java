import java.util.Scanner;

class task6_86{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter n,a,b > ");
	int n = stdin.nextInt(), a = stdin.nextInt(), b = stdin.nextInt(), order = 0;		
	while(n>=10)	{			// order = 1 "a" to the left of "b"					
		n /= 10;				// order = 2 "b" to the left of "a"
		if(n%10==a)	order = 1;
		if(n%10==b)	order = 2;
	}
	if(order==1)	
		System.out.println(a+" to left of "+b);
	else 
		System.out.println(b+" to left of "+a);
}
}