/*
	Удалить из текстового файла всю информацию. Дополнительный файл не
	использовать.
*/

import java.io.*;
 
public class task15_14 {
    public static void main(String[] args) 
		throws IOException
	{  
		try{
			FileWriter fw = new FileWriter("task15_14.txt",true);
			for (int i=0; i<5; i++)
				fw.write("MamaMbI//\\aPamy\n");
			fw.close();
		}
		catch(Exception e){System.out.println(e);}
		try{
			new PrintWriter("task15_14.txt").close();
		}
		catch(Exception e){System.out.println(e);}
    }
}



