/*
	Дан массив. Сравнить первый и второй элементы массива. Если второй
	элемент меньше первого, то поменять их местами. Затем то же самое 
	сделать со вторым и третьим, ..., предпоследним и последним элементами.
	Какое число окажется в результате в последнем элементе массива?
*/

import java.util.Scanner;

class task11_143 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter length of the array >");
		int len = in.nextInt();
		int ar[] = new int[len];
		System.out.print("Enter array elements > ");
		for (int i=0; i<len; i++) {
			ar[i] = in.nextInt();
		}
		for (int i=0; i<len-1; i++) 
			if (Math.abs(ar[i]) > Math.abs(ar[i+1])) 
				ar[i] = ar[i+1] + ar[i] - (ar[i+1] = ar[i]);
		for (int i=0; i<len; i++) 
			System.out.print(ar[i]+" ");
	}
}