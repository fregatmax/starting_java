/*
	В конец существующего текстового файла записать новую строку с текстом
	До свидания, люди!.
*/

import java.io.*;

class task15_5 {
	public static void main(String[] args)
	throws IOException
	{
		try {
			FileWriter fw = new FileWriter("task15_5.txt", true);
			fw.write("\n"+"Goodbye people!");
			fw.close();
		}
		catch (Exception e) {
			System.out.println(e);
		}
	}
}