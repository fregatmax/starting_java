/*
	Таблица футбольного чемпионата, в котором приняли участие 20 команд, задана
	двумерным массивом из одинакового количества строк и столбцов, в котором 
	все элементы, принадлежащие главной диагонали, равны нулю, а каж-
	дый элемент, не принадлежащий главной диагонали, равен 3, 1 или 0 (числу
	очков, набранных в игре: 3 — выигрыш, 1 — ничья, 0 — проигрыш). Название 
	каждой команды известно.
	а) Определить название команд, имеющих больше побед, чем проигрышей.
	б) Определить название команд, которые прошли чемпионат без проигрышей.
	в) Определить название команды, ставшей чемпионом.
	г) Определить, расположены ли команды в соответствии с занятыми ими
	местами в чемпионате, и в случае отрицательного ответа найти название
	первой команды, результаты которой в таблице расположены в нарушение
	такого соответствия (принять, что при равном числе очков места распределяются произвольно);
	д) Получить последовательность названий команд в соответствии с занятыми
	ими местами (сначала должна идти команда, ставшая чемпионом, затем команда,
	занявшая второе место, и т. д.).
*/

import java.util.Scanner;
import java.util.Random;

class Team13_53 {
	String name;
	int score;
	
	Team13_53() {
		java.util.Scanner in = new java.util.Scanner(System.in);
		System.out.println("----------------");
		System.out.print("Enter team's name > ");		
		this.name = in.next();
		this.score = 0;
	}
	public void print() {
		System.out.println(name+" with total score:"+score);
	}
}
class task13_53 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Random rn = new Random();
		final int n = 20;; // amount of teams
		Team13_53[] team = new Team13_53[n];
		int ar[][] = new int[n][n];
		for (int i=0; i<n; i++) {
			team[i] = new Team13_53();
			for (int j=i+1; j<n; j++) {						// get number in range [0;3)	
				int k = rn.nextInt(3);               	   // we need 0,1 or 3 -> if k=2 then
				ar[i][j] = (k == 2 ? 3 : k);	 			//	we write 3
				ar[j][i] = ar[i][j] == 1 ? 1 : 3-ar[i][j];
				// System.out.print((i+1)+" team ("+(n-j)+" numbers) > ");
				// ar[i][j] = in.nextInt();
				// ar[j][i] = ar[i][j] == 1 ? 1 : 3-ar[i][j];
			}										  	 
			ar[i][i] = 0;								
		}
		for (int i=0; i<n; i++)
			for (int j=0; j<n; j++)
				team[i].score += ar[i][j];
		// drawing the table
		if (n>9) {
			System.out.print("        ");
			for (int i=0; i<9; i++)
				System.out.print("  ");
			for(int i=10; i<=n; i++)
				System.out.print((i/10)+" ");
		}
		System.out.print("\n________");
		for (int i=0; i<n; i++)
			System.out.print((i+1)%10+"_");
		System.out.println("\n");
		for (int i=0; i<n; i++) {
			System.out.printf("[%2d] -- ",(i+1));
			for (int j=0; j<n; j++)
				System.out.printf("%d ",ar[i][j]);
			System.out.println();
		}
		int o = 0;
		do {
			System.out.print("\nEnter option (1..5), 0 - exit > ");
			o = in.nextInt();
			switch (o) {
	//а) Определить название команд, имеющих больше побед, чем проигрышей.
				case 1:	{
					System.out.println("Teams with more wins than losses");
					for (int i=0; i<n; i++) {
						int c = 1;
						for (int j=0; j<n; j++)
							if (ar[i][j] == 0 )
								c--;
							else if (ar[i][j] == 3)
								c++;
						if (c > 0)
							System.out.println(team[i].name+" ");
					}		
					}
					break;
	//б) Определить название команд, которые прошли чемпионат без проигрышей.
				case 2: {
					System.out.println("Teams without losses");
					for (int i=0; i<n; i++) {
						boolean f = true;
						for (int j=0; j<n; j++)
							if (i != j & ar[i][j] == 0 )
								f = false;
						if (f)
							System.out.println(team[i].name+" ");
					}
					}
					break;
	//в) Определить название команды, ставшей чемпионом.
				case 3: {
					Team13_53 winner = team[0];
					for (int i=0; i<n; i++)
						if (winner.score < team[i].score)
							winner = team[i];
					System.out.println("Winner - "+winner.name);
					}
					break;
	//г) Определить, расположены ли команды в соответствии с занятыми ими
	// местами в чемпионате, и в случае отрицательного ответа найти название
	// первой команды, результаты которой в таблице расположены в нарушение
	// такого соответствия (принять, что при равном числе очков места распределяются произвольно);
				case 4: {
					boolean f = true;
					first:for (int i=1; i<n; i++)
						if (team[i].score > team[i-1].score) {
							System.out.println("Table is broken by - "+team[i].name);
							f = false;
							break first;
						}
					if (f)
						System.out.println("Teams in the table are arranged according their points");
					}
					break;
	//д) Получить последовательность названий команд в соответствии с занятыми
	// ими местами (сначала должна идти команда, ставшая чемпионом, затем команда,
	// занявшая второе место, и т. д.).
				case 5: {
					int max = team[0].score;
					String newAr[] = new String[n];
					for (int i=0; i<n; i++)
							max = max < team[i].score ? team[i].score : max ;
					int count = 0;
					do {
						int newmax = 0;
						for (int i=0; i<n; i++)
							if (team[i].score == max) {
								newAr[count] = team[i].name;
								System.out.printf("[%2d] - %15s\t score - %d\n",(count+1), newAr[count], team[i].score);
								count++;
							}
							else if ( team[i].score > newmax & team[i].score < max)
								newmax = team[i].score;
						max = newmax;
					} while (count < n);
					}
					break;
			}
		} while (o != 0);
	}
}