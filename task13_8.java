/*
	известны данные о 25 учениках класса: фамилия, имя, отчество, адрес 
	и домашний телефон, если он есть. Вывести на экран фамилию, имя и адрес
	учеников, у которых нет домашнего телефона. Рассмотреть два случая:
	1) телефон задан в виде семизначного числа;
	2) телефон задан в виде, аналогичном следующему: 268–50–59.
*/

class Student13_8 {
	String middlename;
	String lastname;
	String firstname;
	String address;
	int phone;
	
	Student13_8() {
		java.util.Scanner in = new java.util.Scanner(System.in);
		System.out.println("----------------");
		System.out.print("Enter student's firstname > ");
		this.firstname = in.next();
		System.out.print("Enter student's middlename > ");
		this.middlename = in.next();
		System.out.print("Enter student's lastname > ");		
		this.lastname = in.next();
		String temp = in.nextLine();
		System.out.print("Enter student's address > ");		
		this.address = in.nextLine();
		System.out.print("Does this student have a phone? (y/anything)");
		char c = in.nextLine().charAt(0);
		if ( c == 'y') {
			System.out.print("Enter student's phone > ");		
			this.phone = in.nextInt();
		}
		else 
			this.phone = 0;
	}
	public void print() {
	System.out.println(lastname+" "+firstname+" "+address+" "+phone);
	}
}
class task13_8 {
	public static void main(String[] args) {
	final int n = 25; // amount of students
	Student13_8[] st = new Student13_8[n];
	for (int i=0; i<n; i++)
		st[i] = new Student13_8();
	System.out.println("---------------");
	System.out.println("Students with phonenumber");
	for (int i=0; i<n; i++)
		if (st[i].phone != 0)
			st[i].print();
	}
}