/*
	Даны три слова. Выяснить, является ли хоть одно из них палиндромом
	("перевертышем"), т. е. таким, которое читается одинаково слева направо и
	справа налево. (Определить функцию, позволяющую распознавать слова-
	палиндромы.)
*/
import java.util.Scanner;

class task10_32{
	public static void main(String[] args){
		Scanner stdin = new Scanner(System.in);
		String s[] = new String[3];
		System.out.print("Enter 3 words  > ");
		for (int i=0; i<3; i++)
			s[i] = stdin.next();
		if (palindrome(s[0]))
			System.out.println("1st word is palindrome");
		if (palindrome(s[1]))
			System.out.println("2nd word is palindrome");
		if (palindrome(s[2]))
			System.out.println("3rd word is palindrome");			
	}
	static boolean palindrome(String s) {
		if (s.length() < 2)
			return false;
		boolean f = true;
		for (int i=0; i<s.length()/2; i++)
			if (s.charAt(i) != s.charAt(s.length()-i-1))
				f = false;
		return f;
	}
}