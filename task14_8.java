/*
	В конец существующего типизированного файла записать:
	а) число 0;
	б) фразу До свидания!.
*/

import java.io.*;
import java.util.Scanner;
 
public class task14_8 {
    public static void main(String[] args) 
		throws IOException
	{  
		int num = 0;
		Scanner in = new Scanner(System.in);
		do {
		System.out.print("Enter option (1 or 2) > ");
        num = in.nextInt();
		} while (num != 1 & num != 2);
		try{
			FileWriter fw = new FileWriter("task14_8.txt", true);
			switch (num) {
				case 1:
					int outInt = 0;
					fw.write(outInt+'0');
					break;
				case 2:
					String outStr = "Goodbye";
					fw.write(outStr);
					break;
			}
			fw.close();
		}
		catch(Exception e){System.out.println(e);}
    } 
}