// Дано натуральное число. Определить, является ли оно членом последователь-
// ности Фибоначчи
import java.util.Scanner;

class task6_77{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter n (n>1) > ");
	int n = stdin.nextInt(), f1 = 1, f2 = 1;
	while(f2<n)	{
		f2 = f1+(f1=f2);
	}
	if(f2==n) 	System.out.println("Belongs to the Fibonacci sequence");
	else		System.out.println("Doesn't belong to the Fibonacci sequence");
}
}