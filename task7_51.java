import java.util.Scanner;

class task7_51{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter number k > ");
	int k = stdin.nextInt(), order = 0, a;
	for(int i=1;i<=k;i++) {
		System.out.print("Enter a("+i+") > ");
		a = stdin.nextInt();
		if(a<0)	order = i;
	}
	System.out.println("Number of last negative number is "+order);
}
}