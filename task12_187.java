/*
	Составить программу, которая определяет, является ли последовательность 
	элементов побочной диагонали квадратного массива упорядоченной по 
	невозрастанию (при просмотре от правого верхнего угла массива).
	В случае отрицательного ответа должны быть напечатаны координаты 
	первого элемента, нарушающего указанную упорядоченность.
*/

import java.util.Scanner;

class task12_187 {
	public static int[][] rndMatrix(int n, int m) {
		java.util.Random rn = new java.util.Random();
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++)  {
				ar[i][j] = rn.nextInt(20);	
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		return ar;
	}
	public static int[][] enterMatrix(int n, int m) {
		java.util.Scanner in = new java.util.Scanner(System.in);
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			System.out.print("Enter "+(i+1)+" row elements > ");
			for (int j=0; j<n; j++)
				ar[i][j] = in.nextInt();
			String temp = in.nextLine();
		}
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++) 
				System.out.printf("%3d ",ar[i][j]);
			System.out.println();
		}
		return ar;
	}
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter n (height and width) of 2D-array > ");
		final int n = in.nextInt();
		int ar[][] = enterMatrix(n,n);
		for (int i=1; i<n; i++)
			if (ar[i][n-1-i] > ar[i-1][n-i]) {
				System.out.println("["+(i+1)+"]["+(n-i)+"] disrupts the sequence");
				return;
			}
		System.out.println("Elements of the secondary diagonal are non-ascending");
	}
}