/*
	Составить процедуру, "рисующую" на экране вертикальную линию
	из любого числа символов "*"
*/
import java.util.Scanner;

class task10_36{
	public static void main(String[] args){
		Scanner stdin = new Scanner(System.in);
		System.out.print("Enter n  > ");
		int n = stdin.nextInt();
		drawStars(n);
	}
	static void drawStars(int n) {
		for (int i=0; i<n-1; i++) 
			System.out.println("*");
		System.out.print("*");
	}
}