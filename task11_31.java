/*
	В массиве хранится информация о численности книг в каждом
	из 35 разделов библиотеки. Выяснить, верно ли, что общее
	число книг в библиотеке	есть шестизначное число.
*/

class task11_31 {
	public static void main(String[] args) {
		int ar[] = new int[35];
		System.out.println("35 random number < 10 000");
		for (int i=0; i<ar.length; i++) {
			ar[i] = (int)(Math.random()*10000);
			System.out.print(ar[i]+" ");
		}
		System.out.println("\n----------------------------------------");
		int sum = 0;
		for (int i=0; i<ar.length; i++)
			sum += ar[i];
//		System.out.println(sum);
		if (sum > 99999 & sum < 1000000)
			System.out.print("True");
		else
			System.out.print("False");
	}
}