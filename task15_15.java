/*
	имеется текстовый файл. Добавить в него строку из двенадцати черточек
	(------------), разместив ее:
	а) после пятой строки;
	б) после последней из строк, в которых нет пробела. Если таких строк нет, то
	новая строка должна быть добавлена после всех строк имеющегося файла.
	В обоих случаях результат записать в другой файл.
*/

import java.io.*;
import java.util.Scanner;
 
public class task15_15 {
    public static void main(String[] args) 
		throws IOException
	{  
		String temp;
		Scanner stdin = new Scanner(System.in);
		try{
			Scanner in = new Scanner(new File("task15_15_in.txt"));
			FileWriter fw = new FileWriter("task15_15_out.txt");
			int o = 0;
			do {
			System.out.print("Eneter option (1 or 2) > ");
			o = stdin.nextInt();
			} while (o != 1 & o != 2);
			switch (o) {
				case 1:
					int count = 0;
					while (in.hasNextLine()) {
						temp = in.nextLine();
						if (count++ == 5)
							fw.write("------------\n");
						fw.write(temp+"\n");
					}
					break;
				case 2:
					int count2 = 0;
					boolean fl = false;
					for(int i=1; in.hasNextLine(); i++) {
						temp = in.nextLine();
						if (temp.contains(" ")) {
							fl = true;
							count2 = i;
						}
					}
					in = new Scanner(new File("task15_15_in.txt"));
					while (in.hasNextLine()) {
						temp = in.nextLine();
						if (fl)
							if (count2-- == 1)
								fw.write(temp+"\n------------\n");
							else
								fw.write(temp+"\n");
						else
							fw.write(temp+"\n------------\n");
					}
					break;
			}
			fw.close();
		}
		catch(Exception e){System.out.println(e);}
    }
}