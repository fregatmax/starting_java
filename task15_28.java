/*
	имеется текстовый файл. Переписать в другой файл все его строки с заменой
	в них символа 0 на символ 1 и наоборот
*/

import java.util.Scanner;
import java.io.*;

class task15_28 {
	public static void main(String[] args)
	throws IOException
	{
		char ar[];
		try {
			Scanner in = new Scanner(new File("task15_28_in.txt"));
			FileWriter fw = new FileWriter("task15_28_out.txt");
			while (in.hasNextLine()) {
				ar = in.nextLine().toCharArray();
				for (int i=0; i<ar.length; i++)
					if (ar[i] == '1')
						fw.write('0');
					else if (ar[i] == '0')
						fw.write('1');
					else
						fw.write(ar[i]);
				fw.write("\n");
			}
			fw.close();
		}
		catch (Exception e) { System.out.println(e); }
	}
}