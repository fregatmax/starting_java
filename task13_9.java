/*
	известна информация о 30 клиентах пункта проката: фамилия, имя, отчество,
	адрес и домашний телефон. известно также название предмета, взятого
	каждым из них напрокат (в виде: т — телевизор, х — холодильник и т. п.).
	Вывести на экран фамилию, имя и адрес клиентов, взявших напрокат телевизор.
*/

class Customer13_9 {
	String middlename;
	String lastname;
	String firstname;
	String address;
	int phone;
	char item;
	
	Customer13_9() {
		java.util.Scanner in = new java.util.Scanner(System.in);
		System.out.println("----------------");
		System.out.print("Enter customer's firstname > ");
		this.firstname = in.next();
		System.out.print("Enter customer's middlename > ");
		this.middlename = in.next();
		System.out.print("Enter customer's lastname > ");		
		this.lastname = in.next();
		String temp = in.nextLine();
		System.out.print("Enter customer's address > ");		
		this.address = in.nextLine();
		System.out.print("Enter customer's phone > ");
		this.phone = in.nextInt();
		temp = in.nextLine();
		System.out.print("What customer rented? (t - TV, f - freezer) > ");
		this.item = in.nextLine().charAt(0);
	}
	public void print() {
	System.out.println(lastname+" "+firstname+" "+address+" "+phone);
	}
}
class task13_9 {
	public static void main(String[] args) {
	final int n = 30; // amount of customers
	Customer13_9[] cus = new Customer13_9[n];
	for (int i=0; i<n; i++)
		cus[i] = new Customer13_9();
	System.out.println("---------------");
	System.out.println("customers who rented freezer");
	for (int i=0; i<n; i++)
		if (cus[i].item == 'f')
			cus[i].print();
	}
}