/*
	Определить, является ли массив упорядоченным по возрастанию. В случае
	отрицательного ответа определить номер первого элемента, нарушающего
	такую упорядоченность.
*/

import java.util.Scanner;

class task11_209 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter length of the array >");
		int len = in.nextInt();
		int ar[] = new int[len];
		for (int i=0; i<ar.length; i++) {
			ar[i] = (int)(Math.random()*(5)+i);
			System.out.print(ar[i]+" ");
		}
		for (int i=1; i<ar.length; i++)
			if (ar[i-1] > ar[i]) {
				System.out.println("\nNot ascending from "+(i+1));
				return;
			}
		System.out.println("\nAscending");
	}
}