/*
	Написать рекурсивную процедуру для ввода с клавиатуры
	последовательности чисел и вывода ее на экран в обратном
	порядке (окончание последовательности — при вводе нуля).
*/
import java.util.Scanner;

class task10_53{
	public static void main(String[] args){
		readWriteRec();
	}
	static void readWriteRec() { 
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		if (n == 0)
			return;
		else
			readWriteRec();
		System.out.print(n);
		return;
	}
}