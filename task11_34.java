/*
	В массиве хранятся сведения о количестве осадков, выпавших за каждый
	день июня. Определить в какой период выпало больше осадков: 
	в первую половину июня или во вторую;
*/

class task11_34 {
	public static void main(String[] args) {
		int ar[] = new int[30];
		System.out.println("Precipitation at each day in June");
		for (int i=0; i<ar.length; i++) {
			ar[i] = (int)(Math.random()*100);
			System.out.print(ar[i]+" ");
		}
		System.out.println("\n----------------------------------------");
		int sum1, sum2;
		sum1 = sum2 = 0;
		for (int i=0; i<ar.length/2; i++) {
			sum1 += ar[i];
			sum2 += ar[ar.length-i-1];
		}
		System.out.println(sum1+" "+sum2);
		if (sum1 > sum2)
			System.out.print("First half of June was more rainy");
		else
			System.out.print("Second half of June was more rainy");
	}
}
