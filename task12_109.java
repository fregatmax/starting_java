/*
	Дан двумерный массив из пятнадцати строк и двух столбцов. Найти номера
	двух соседних строк, сумма элементов в которых минимальна.
*/

class task12_109 {
	public static void main(String[] args) {
		final int n = 2, m = 15;
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			System.out.print("["+(i+1)+"]\t");
			for (int j=0; j<n; j++) {
				ar[i][j] = (int)(Math.random()*10);	
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		System.out.println("\n----------------------------------------");
		int min = ar[0][0] + ar[0][1] + ar[1][0] + ar [1][1];
		int minrow = 1;
		for (int i=2; i<m; i++) {
			int t = 0;
			for (int j=0; j<n; j++)
				t += ar[i][j]+ar[i-1][j];
			if (min > t) {
				min = t;
				minrow = i;
			}
		}
		System.out.println("Min sum = "+min+" at rows "+minrow+" and "+(minrow+1));
	}
}		