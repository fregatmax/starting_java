/*
	Переставить s-й элемент массива на место k-го элемента (s > k). При этом k-й,
	(k + 1)-й, ..., (s – 1)-й элементы сдвинуть вправо на 1 позицию.
*/

import java.util.Scanner;

class task11_175 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter length of the array >");
		int len = in.nextInt();
		int ar[] = new int[len];
		System.out.print("Enter array elements > ");
		for (int i=0; i<len; i++) {
			ar[i] = in.nextInt();
		}
		int s,k;
		do {
		System.out.print("Enter s, k (s>k) > ");
		s = in.nextInt()-1; k = in.nextInt()-1; // cause starting at 0
		} while(s < k);
		int t = ar[s];
		for (int i=k; i<=s; i++)
			ar[i] = t + ar[i] - (t = ar[i]);
		System.out.println();
		for (int i=0; i<len; i++) 
			System.out.print(ar[i]+" ");
	}
}