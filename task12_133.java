/*
	Определить, имеются ли в двумерном массиве только два одинаковых элемента.
*/

import java.util.Scanner;

class task12_133 {
	public static int[][] rndMatrix(int n, int m) {
		java.util.Random rn = new java.util.Random();
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++)  {
				ar[i][j] = rn.nextInt(20);	
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		return ar;
	}
	public static int[][] enterMatrix(int n, int m) {
		java.util.Scanner in = new java.util.Scanner(System.in);
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			System.out.print("Enter "+(i+1)+" row elements > ");
			for (int j=0; j<n; j++)
				ar[i][j] = in.nextInt();
			String temp = in.nextLine();
		}
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++) 
				System.out.printf("%3d ",ar[i][j]);
			System.out.println();
		}
		return ar;
	}
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter n(width) and m(height) of 2D-array separated by space > ");
		final int n = in.nextInt(), m = in.nextInt();
		int ar[][] = enterMatrix(n,m);
		boolean f = false;
		start:for (int i=0; i<m; i++) 
			for (int j=0; j<n; j++)
				for (int k=i; k<m; k++)
					for (int l=(k==i?(j+1):0); l<n; l++)
						if (ar[i][j] == ar[k][l])
							if (!f)
								f = true;
							else {
								f = false;
								break start;
							}
		if (f)
			System.out.println("There are only two same elements in matrix");
		else
			System.out.println("There are not only two same elements in matrix");
				
	}
}