import java.util.Scanner;

class task6_20{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter a (a!=1),x and epsilon separated by spaces > ");
	double a = stdin.nextDouble(), x = stdin.nextDouble(), eps = stdin.nextDouble();
	double y = 0.5*(a+x/(a-1));
	int i = 1;
	while( Math.abs(y*y-a*a)>eps)	{
		y = 0.5*((a=y)+x/(a-1));
		i++;
	}
	System.out.print("y = "+y+" n = "+i);
}
}