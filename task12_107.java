/*
	Дан двумерный массив из двух строк и пятнадцати столбцов. Найти номера
	двух соседних столбцов, сумма элементов в которых минимальна.
*/

class task12_107 {
	public static void main(String[] args) {
		final int n = 15, m = 2;
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++) {
				ar[i][j] = (int)(Math.random()*10);	
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		System.out.println("\n----------------------------------------");
		int min = ar[0][0] + ar[1][0] + ar[0][1] + ar [1][1];
		int mincol = 1;
		for (int j=2; j<n; j++) {
			int t = 0;
			for (int i=0; i<m; i++)
				t += ar[i][j]+ar[i][j-1];
			if (min > t) {
				min = t;
				mincol = j;
			}
		}
		System.out.println("Min sum = "+min+" at columns "+mincol+" and "+(mincol+1));
	}
}		