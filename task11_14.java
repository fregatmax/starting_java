/*
	Вывести элементы массива на экран в обратном порядке
*/

class task11_14 {
	public static void main(String[] args) {
		int ar[] = new int[20];
		System.out.println("20 first numbers divisible by 13 and 17");
		for (int i=0, j=1; i<20; i++, j++) {
			for (; j<301; j++)
				if(j%13 == 0 | j%17 == 0)
					break;
			ar[i] = j;
			System.out.print(ar[i]+" ");
		}
		System.out.println("\n----------------------------------------");
		for (int i=ar.length-1; i>=0; i--)
			System.out.print(ar[i]+" ");
	}
}