/*
	Строка содержит арифметическое выражение, в котором используются
	круглые скобки, в том числе вложенные. Проверить,
	правильно ли в нем расставлены скобки.
	Ответом должны служить слова да или нет.
*/
import java.util.Scanner;

class task9_185{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter text > ");
	String s1 = stdin.nextLine();
	int count = 0;
	for (int i=0; i<s1.length(); i++) {
		switch (s1.charAt(i)) {
			case '(':
				count++;
				break;
			case ')':
				count--;
				break;
		}
		if (count < 0) {
			System.out.println("No, extra right parenthesis at "+(i+1));
			return;
		}
	}
	if (count == 0)
		System.out.println("Yes");
	else
		System.out.println("No, "+count+" extra left parentheses");
}
}