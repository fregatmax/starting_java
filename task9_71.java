import java.util.Scanner;

class task9_71{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter sentence > ");
	String s = stdin.nextLine();
	int m = 0, n = 0;
	for(int i=0;i<s.length();i++)
		switch(s.charAt(i)){
			case 'n':
			case 'N':
				n++;
				break;
			case 'm':
			case 'M': 
				m++;
				break;
		}
	System.out.println("\"n\" - "+n+"\t\t\"m\" - "+m);
}
}