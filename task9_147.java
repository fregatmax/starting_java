/* 
	Дан текст, имеющий вид:" d1±d2±..±dn , где di — цифры (n > 1). 
	Вычислить записанную в тексте алгебраическую сумму.
*/
import java.util.Scanner;

class task9_147{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter algebraic expression (d1±d2±...dn > ");
	String e = stdin.nextLine();
	int s = Character.getNumericValue(e.charAt(0));
	//System.out.println(e);
	for (int i=2; i<e.length(); i+= 2)
		if (e.charAt(i-1) == '+')
			s += Character.getNumericValue(e.charAt(i));
		else if(e.charAt(i-1) == '-')
			s -= Character.getNumericValue(e.charAt(i));
		else {
			System.out.println("Bad expression");
			return;
		}
	System.out.println(s);	
}
}