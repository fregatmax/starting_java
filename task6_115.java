class task6_115{
public static void main(String[] args){
	double a,b,c,eps;
	a = 0.0;
	b = 1.0;
	c = 0;
	eps = 0.001;
	int count = 0;
	while(b-a > eps){
		c = (a+b)/2;
		count++;
		if((a*a*a*a+2*a*a*a-a-1)*(c*c*c*c+2*c*c*c-c-1) < 0)	b = c;
		else a = c;
	}
	System.out.println("x > "+c+"  iterations > "+count);
	
	a = 1.0;
	b = 1.5;
	count = 0;
	while(b-a > eps){
		c = (a+b)/2;
		count++;
		if((a*a*a-0.2*a*a-0.2*a-1.2)*(c*c*c-0.2*c*c-0.2*c-1.2) < 0)	b = c;
		else a = c;
	}
	System.out.println("x > "+c+"  iterations > "+count);
}
}