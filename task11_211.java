/*
	имеются данные о сумме очков, набранных в чемпионате каждой
	из футбольных команд. Определить, перечислены ли команды
	в списке в соответствии с занятыми ими местами в чемпионате.
*/

import java.util.Scanner;

class task11_211 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter length of the array >");
		int len = in.nextInt();
		int ar[] = new int[len];
		for (int i=0; i<ar.length; i++) {
			ar[i] = (int)(Math.random()*10+20-i*2);
			System.out.print(ar[i]+" ");
		}
		for (int i=1; i<ar.length; i++)
			if (ar[i-1] < ar[i]) {
				System.out.println("\nWrong order");
				return;
			}
		System.out.println("\nRight order");
	}
}