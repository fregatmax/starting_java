/*
	известны данные о росте 15-ти юношей класса, упорядоченные по убыванию.
	Нет ни одной пары учеников, имеющих одинаковый рост. В начале
	учебного года в класс поступил новый ученик (известно, что его рост не 
	совпадает с ростом ни одного из учеников класса, превышает рост самого 
	низкого ученика и меньше роста самого высокого).
	а) Вывести фамилии всех учеников, рост которых меньше роста "новенького".
	б) Определить фамилию ученика, после которого следует записать фамилию
	"новенького", чтобы упорядоченность не нарушилась.
	в) Определить фамилию ученика, рост которого меньше всего отличается от
	роста "новенького".
	В задачах (а) и (б) условный оператор не использовать.
*/

class Student13_45 {
	int height;
	String lastname;
	
	Student13_45() {
		java.util.Scanner in = new java.util.Scanner(System.in);
		System.out.println("----------------");
		System.out.print("Enter student's lastname > ");		
		this.lastname = in.next();
		String temp = in.nextLine();
		System.out.print("Enter student's height > ");		
		this.height = in.nextInt();
	}
	public void print() {
		System.out.println(lastname+" with height:"+height);
	}
}
class task13_45 {
	public static void main(String[] args) {
		final int n = 5; // amount of students
		Student13_45[] st = new Student13_45[n];
		for (int i=0; i<n; i++)
			st[i] = new Student13_45();
		System.out.println("------------------");
		System.out.println("New kid");
		Student13_45 kid = new Student13_45();
		int count = n-1;
		System.out.println("\nSmaller students than new one:");
		while (st[count].height < kid.height)
			st[count--].print();
		System.out.println("\nNew kid should be write down after "+st[count].lastname);
		System.out.println("------------------");
		System.out.print("Student with the closest height - ");
		if (st[count].height-kid.height < kid.height-st[count+1].height)
			System.out.println(st[count].lastname);
		else
			System.out.println(st[count+1].lastname);
	}
}