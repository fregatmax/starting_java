class task3_24 {
public static void main(String[] args) {
	boolean X = false, Y = false, Z = false;
	System.out.println("X\tY\tZ\t!(Y||!X&&Z)||Z");
	for(int i=0;i<2;i++) {
		X = !X;
		for(int j=0;j<2;j++) {
			Y = !Y;
			for(int k=0;k<2;k++) {
				Z = !Z;
				System.out.println(X+"\t"+Y+"\t"+Z+"\t"+(!(Y||!X&&Z)||Z));
			}
		}
	}
	System.out.println("-------------------------\nX\tY\tZ\tX&&!(!Y||Z)||Y");
	for(int i=0;i<2;i++) {
		X = !X;
		for(int j=0;j<2;j++) {
			Y = !Y;
			for(int k=0;k<2;k++) {
				Z = !Z;
				System.out.println(X+"\t"+Y+"\t"+Z+"\t"+(X&&!(!Y||Z)||Y));
			}
		}
	}
	System.out.println("-------------------------\nX\tY\tZ\t!(X||Y&&Z)||!X");
	for(int i=0;i<2;i++) {
		X = !X;
		for(int j=0;j<2;j++) {
			Y = !Y;
			for(int k=0;k<2;k++) {
				Z = !Z;
				System.out.println(X+"\t"+Y+"\t"+Z+"\t"+(!(X||Y&&Z)||!X));
			}
		}
	}		
}
}