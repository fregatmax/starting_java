/*
	Оценки, полученные спортсменом в соревнованиях по фигурному катанию
	(в баллах), хранятся в массиве из 18 элементов. В первых шести элементах
	записаны оценки по обязательной программе; седьмом, ..., двенадцатом —
	по короткой программе; в остальных — по произвольной программе. Выяснить,
	по какому виду программы спортсмен показал лучший результат.
*/

class task11_35 {
	public static void main(String[] args) {
		int ar[] = new int[18];
		System.out.println("Points at each discipline");
		for (int i=0; i<ar.length; i++) {
			ar[i] = (int)(Math.random()*10);
			System.out.print(ar[i]+" ");
		}
		System.out.println("\n----------------------------------------");
		int max = 0, f = 0;
		for (int i=0; i<3; i++) {
			int t = 0;
			for (int j=i*6; j<i*6+5; j++)
				t += ar[i];
			if (t > max) {
				max = t;
				f = i;
			}
		}
		switch (f) {
			case 0:
				System.out.println("Mandatory program (1)");
				break;
			case 1:
				System.out.println("Short program (2)");
				break;
			case 2:
				System.out.println("Free skating (3)");
				break;
		}
	}
}