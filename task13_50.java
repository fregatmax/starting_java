/*
	известно количество очков, набранных каждой из 19 команд-участниц 
	первенства по футболу. Перечень очков и команд дан в порядке занятых ими
	мест, т. е. в порядке убывания количества набранных очков (ни одна пара
	команд-участниц не набрала одинаковое количество очков). Выяснилось,
	что в перечень забыли включить еще одну, двадцатую, команду. Получить
	новый список команд (с учетом дополнительной команды), в котором 
	команды также расположены в порядке убывания количества набранных ими
	очков.
*/

class Team13_50 {
	int score;
	String name;
	
	Team13_50() {
		java.util.Scanner in = new java.util.Scanner(System.in);
		System.out.println("----------------");
		System.out.print("Enter team's name > ");		
		this.name = in.next();
		String temp = in.nextLine();
		System.out.print("Enter team's score > ");		
		this.score = in.nextInt();
	}
	public void print() {
		System.out.println(name+" with total score:"+score);
	}
}
class task13_50 {
	public static void main(String[] args) {
		int n = 5; // amount of teams
		Team13_50[] st = new Team13_50[n];
		for (int i=0; i<n; i++)
			st[i] = new Team13_50();
		System.out.println("------------------");
		System.out.println("Forgotten team");
		Team13_50 team = new Team13_50();
		Team13_50[] st1 = new Team13_50[n+1];
		System.out.println("------------------");		
		int count = 0;
		while (st[count].score > team.score)
			st1[count] = st[count++];
		st1[count++] = team;
		for ( ; count<n+1; count++)
			st1[count] = st[count-1];
		n += 1;
		System.out.println("------------------");
		for (int i=0; i<n; i++)
			st1[i].print();
	}
}