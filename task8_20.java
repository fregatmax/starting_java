import java.util.Scanner;

class task8_20{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	int max = 0, total, store = 0;;
	// а
	for(int i=1;i<=3;i++) {
		total = 0;
		System.out.println("Store number "+i);
		System.out.print("Enter income for 10 days separated be spaces: ");
		for(int j=1;j<=10;j++)
			total += stdin.nextInt();
		if(max < total) {
			max = total;
			store = i;
		}
	}
	System.out.println("Maximum income - "+max+" in store number "+store);
	// б и в
	int day = 0, max2 = 0, daystore = 0, income;
	for(int i=1;i<=10;i++) {
		total = 0;
		System.out.println("Day number "+i);
		System.out.print("Enter income for 1st, 2nd and 3rd stores separated by spaces: ");
		for(int j=1;j<=3;j++) {
			income = stdin.nextInt();
			if(income > max2) {
				daystore = i*10+j;
				max2 = income;
			}
			total += income;
			}
		if(max < total) {
			max = total;
			day = i;
		}
	}
	System.out.println("Maximum income - "+max+" at day number "+day);
	System.out.println("Maximum income - "+max2+" at day number "+(daystore/10)+" in store number "+(daystore%10));
}
}