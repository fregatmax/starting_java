/*
	Даны два массива одного размера. Получить третий массив, каждый эле-
	мент которого равен:
	а) сумме элементов с тем же номером в заданных массивах;
	б) произведению элементов с тем же номером в заданных массивах;
	в) максимальному из элементов с тем же номером в заданных массивах.
*/

import java.util.Scanner;

class task11_246 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter length of the array >");
		int len = in.nextInt();
		int ar1[] = new int[len], ar2[] = new int[len];
		System.out.print("Enter 1st array elements > ");
		for (int i=0; i<len; i++) 
			ar1[i] = in.nextInt();
		System.out.print("Enter 2nd array elements > ");
		for (int i=0; i<len; i++) 
			ar2[i] = in.nextInt();
		int o;
		do {
			System.out.print("\nEnter option (1..3), 0 - exit >");
			int[] ar3 = new int[ar1.length];
			o = in.nextInt();
			switch (o) {
				case 1: 
					for (int i=0; i<ar1.length; i++)
						ar3[i] = ar1[i] + ar2[i];
					break;
				case 2:
					for (int i=0; i<ar1.length; i++)
						ar3[i] = ar1[i] * ar2[i];
					break;
				case 3:
					for (int i=0; i<ar1.length; i++)
						ar3[i] = ar1[i] > ar2[i] ? ar1[i] : ar2[i];
					break;
			}
			System.out.print("1st: ");
			for (int i=0; i<len; i++) 
				System.out.print(ar1[i]+" ");
			System.out.print("\n2nd: ");
			for (int i=0; i<ar1.length; i++) 
				System.out.print(ar2[i]+" ");
			System.out.print("\n3rd: ");
			for (int i=0; i<ar2.length; i++) 
				System.out.print(ar3[i]+" ");
		} while (o != 0 );
	}
}
