import java.util.Scanner;

class task2_39 {
public static void main(String[] args) {
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter h(1..23), m(0..59), s(0..59) separated by a space > ");
	byte h = stdin.nextByte(), m = stdin.nextByte(), s = stdin.nextByte();
	double d=0;
	d = h%12*30+m*0.5+s/120.0;
	System.out.print(d+" degrees");
}
}