import java.util.Scanner;

class task5_61{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter number of pupils > ");
	int n = stdin.nextInt();
	double m;
	for(int i=1; i<3; i++)	{
		m = 0;
		if(i==1)	System.out.println("1st class");
		else		System.out.println("2nd class");
		for(int j=1;j<n+1;j++)	{
			System.out.print("Enter "+j+" height of pupil >");
			m += stdin.nextInt();
			}
		m = m/n;
		System.out.printf("Average height is > %3.2f\n", m);
	}
}
}