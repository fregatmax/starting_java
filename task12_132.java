/*
	Определить, имеются ли в двумерном массиве одинаковые элементы.
*/

import java.util.Scanner;
import java.util.Random;

class task12_132 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter n(width) and m(height) of 2D-array separated by space > ");
		final int n = in.nextInt(), m = in.nextInt();
		Random rn = new Random();
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++)  {
				ar[i][j] = rn.nextInt(20);	
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		boolean f = false;
		for (int i=0; i<m; i++) 
			for (int j=0; j<n; j++)
				for (int k=i; k<m; k++)
					for (int l=(k==i?(j+1):0); l<n; l++)
						f = ar[i][j] == ar[k][l] ? true : f;
		if (f)
			System.out.println("There are same elements in matrix");
		else
			System.out.println("There are not same elements in matrix");
				
	}
}