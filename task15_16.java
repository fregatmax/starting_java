/*
	имеется текстовый файл. Напечатать:
	а) его первую строку;
	б) его пятую строку;
	в) его первые 5 строк;
	г) его строки с s1-й по s2-ю;
	д) весь файл.
*/

import java.io.*;
import java.util.Scanner;
 
public class task15_16 {
    public static void main(String[] args) 
		throws IOException
	{  
		try {
			String temp;
			Scanner in = new Scanner(new File("task15_16.txt"));
			Scanner stdin = new Scanner(System.in);
			int o = 0;
			do {
			System.out.print("Eneter option (1-5) > ");
			o = stdin.nextInt();
			} while (o < 1 & o > 5);
			switch (o) {
				case 1:
					System.out.println(in.nextLine());
					break;
				case 2:
					for(int i=1; in.hasNextLine() & i<5; i++)
							in.nextLine();
					System.out.println(in.nextLine());
					break;
				case 3:
					for(int i=0; in.hasNextLine() & i<5; i++)
						System.out.println(in.nextLine());
					break;
				case 4:	
					int s1 = 0, s2 = 0;
					do {
						System.out.print("Eneter s1 and s2 (s1<s2) > ");
						s1 = stdin.nextInt();
						s2 = stdin.nextInt();
					} while (s1 > s2);
					for(int i=1; in.hasNextLine() & i<s1; i++)
						in.nextLine();
					for(int i=s1; in.hasNextLine() & i<=s2; i++)
						System.out.println(in.nextLine());
					break;
				case 5:
					while (in.hasNextLine())
						System.out.println(in.nextLine());
					break;
			}
		}
		catch (Exception e) { System.out.println(e); }
	}
}