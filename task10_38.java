/*
	Составить программу, в результате которой величина а меняется
	значением с величиной b, а величина c — с величиной d.
	(Определить процедуру, осуществляющую обмен значениями двух
	переменных величин.)
*/
import java.util.Scanner;

class task10_38{
	int a;
	public static void main(String[] args){
		Scanner stdin = new Scanner(System.in);
		System.out.print("Enter a and b > ");
		task10_38 a = new task10_38(stdin.nextInt());
		task10_38 b = new task10_38(stdin.nextInt());
		System.out.print("Enter c and d > ");
		task10_38 c = new task10_38(stdin.nextInt());
		task10_38 d = new task10_38(stdin.nextInt());
		System.out.println("a - "+a.a+"  b - "+b.a);
		swapVar(a,b);
		System.out.println("a - "+a.a+"  b - "+b.a);
		System.out.println();
		System.out.println("c - "+c.a+"  d - "+d.a);
		swapVar(c,d);
		System.out.println("c - "+c.a+"  d - "+d.a);
	}
	public task10_38(int a) {
		this.a = a;
	}
	static void swapVar(task10_38 a, task10_38 b) {
		a.a ^= (b.a ^= a.a);
		b.a ^= a.a; 
	}
}