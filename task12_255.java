/*
	Дан двумерный массив из пяти строк и двадцати столбцов. Перенести 
	первые s столбцов в конец массива, соблюдая порядок их следования.
*/

import java.util.Scanner;

class task12_255 {
	public static int[][] rndMatrix(int n, int m) {
		java.util.Random rn = new java.util.Random();
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++)  {
				ar[i][j] = rn.nextInt(20);	
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		return ar;
	}
	public static void printMatrix(int[][] ar) {
		for (int i=0; i<ar.length; i++) {
			for (int j=0; j<ar[i].length; j++) 
				System.out.printf("%3d ",ar[i][j]);
			System.out.println();
		}
	}
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		final int n = 20, m = 5;
		int ar[][] = rndMatrix(n,m);
		System.out.print("Enter s > ");
		int s = in.nextInt();
		for (int l=s; l>0; l--) {
			for (int j=1; j<n; j++)
				for (int i=0; i<m; i++)
					ar[i][j] = ar[i][j-1] + ar[i][j] - (ar[i][j-1] = ar[i][j]);
		}
		System.out.println("----------------------------------------");
		printMatrix(ar);
	}
}