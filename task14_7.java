/*
	Дана последовательность целых чисел, оканчивающаяся нулем. Записать все
	числа последовательности в типизированный файл.
*/

import java.io.*;
import java.util.Scanner;
 
public class task14_7 {
    public static void main(String[] args) 
		throws IOException
	{  
		Scanner in = new Scanner(System.in);
		System.out.print("Enter numbers (0 - exit) > ");
        int num;
		try{
			PrintWriter pw = new PrintWriter(new File("task14_7.txt"));
			do {
			num = in.nextInt();
			pw.print(num+" ");
			} while (num != 0);
			pw.close();
		}
		catch(Exception e){e.printStackTrace();}
    } 
}