/*
	имеются два текстовых файла с одинаковым числом строк. Выяснить, сов-
	падают ли их строки. Если нет, то получить номер первой строки, в которой
	эти файлы отличаются друг от друга.
*/

import java.util.Scanner;
import java.io.*;

class task15_31 {
	public static void main(String[] args)
	throws IOException
	{
		try {
			Scanner in1 = new Scanner(new File("task15_31_1.txt"));
			Scanner in2 = new Scanner(new File("task15_31_2.txt"));
			int count = 0;
			boolean fl = true;
			while (in1.hasNextLine()) {
				count++;
				if (!in1.nextLine().equals(in2.nextLine())) {
					System.out.println("First not equal string number - "+count);
					fl = false;
					break;
				}
			}
			if (fl)
				System.out.println("Files are equal");
		}
		catch (Exception e) { System.out.println(e); }
	}
}