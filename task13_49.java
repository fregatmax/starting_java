/*
	известны данные о росте 15-ти юношей класса, упорядоченные по убыванию. 
	Нет ни одной пары учеников, имеющих одинаковый рост. В начале
	учебного года в класс поступил новый ученик (известно, что его рост не 
	совпадает с ростом ни одного из учеников класса, превышает рост самого 
	низкого ученика и меньше роста самого высокого). Получить новый список 
	фамилий учеников (с учетом фамилии "новенького"), в котором фамилии 
	расположены в порядке убывания роста.
*/

class Student13_49 {
	int height;
	String lastname;
	
	Student13_49() {
		java.util.Scanner in = new java.util.Scanner(System.in);
		System.out.println("----------------");
		System.out.print("Enter student's lastname > ");		
		this.lastname = in.next();
		String temp = in.nextLine();
		System.out.print("Enter student's height > ");		
		this.height = in.nextInt();
	}
	public void print() {
		System.out.println(lastname+" with height:"+height);
	}
}
class task13_49 {
	public static void main(String[] args) {
		int n = 5; // amount of students
		Student13_49[] st = new Student13_49[n];
		for (int i=0; i<n; i++)
			st[i] = new Student13_49();
		System.out.println("------------------");
		System.out.println("New kid");
		Student13_49 kid = new Student13_49();
		Student13_49[] st1 = new Student13_49[n+1];
		System.out.println("------------------");		
		int count = 0;
		while (st[count].height > kid.height)
			st1[count] = st[count++];
		st1[count++] = kid;
		for ( ; count<n+1; count++)
			st1[count] = st[count-1];
		n += 1;
		System.out.println("------------------");
		for (int i=0; i<n; i++)
			st1[i].print();
	}
}