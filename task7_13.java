import java.util.Scanner;

class task7_13{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter n > ");
	int n = stdin.nextInt(), count=1, sum=0, evensum=0, divcount=0;
	System.out.print("Dividers: ");
	while(count<n+1){
		if(n%count==0)	{
			System.out.print(count+" ");
			sum += count;
			if(count%2==0)	evensum += count;
			divcount++;
			}
		count++;
	}
	System.out.println("\nDividers sum > "+sum+" Even dividers sum > "+evensum);
	System.out.println("Number of dividers > "+divcount);
}
}