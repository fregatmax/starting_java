/*
	Дан двумерный массив целых чисел. Определить:
	а) есть ли в нем столбец, состоящий только из нулей;
	б) есть ли в нем столбец, состоящий только из элементов, принадлежащих
	промежутку от 0 до b;
	в) есть ли в нем столбец, состоящий только из четных элементов;
	г) есть ли в нем столбец, в котором равное количество положительных
	и отрицательных элементов;
	д) есть ли в нем столбец, в котором имеются одинаковые элементы;
	е) есть ли в нем столбец, в котором имеются как минимум три элемента, яв-
	ляющиеся минимальными в массиве.
*/
import java.util.Scanner;
import java.util.Random;

class task12_153 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Random rn = new Random();
		System.out.print("Enter n(width) and m(height) of 2D-array separated by space > ");
		int n = in.nextInt(), m = in.nextInt();
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++) {
				ar[i][j] = rn.nextInt(5)-2;		
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		int o = 0;
		do {
			System.out.print("\nEnter option (1..6), 0 - exit > ");
			o = in.nextInt();
			switch (o) {
	//а) есть ли в нем столбец, состоящий только из нулей;
				case 1:	{
					boolean f =false;
					for (int j=0; j<n; j++) {
						boolean fl = true;
						for (int i=0; i<m; i++)
							fl = ar[i][j] == 0 ? fl : false;
						if (fl)
							f =true;
					}		
					if (f)
						System.out.println("There is a column consisting only zeros");
					else
						System.out.println("There is not a column consisting only zeros");
					}
					break;
	//б) есть ли в нем столбец, состоящий только из элементов, принадлежащих
	// промежутку от 0 до b;
				case 2: {
					System.out.print("Enter b > ");
					int b = in.nextInt();
					boolean f = false;
					for (int j=0; j<n; j++) {
						boolean fl = true;
						for (int i=0; i<m; i++)
							if (ar[i][j] > b | ar[i][j] < 0)
								fl = false;
						if (fl)
							f = true;
					}
					if (f)
						System.out.println("There is a column consisting only numbers from [0,"+b+"]");
					else
						System.out.println("There is not a column consisting only numbers from [0,"+b+"]");
					}
					break;
	//в) есть ли в нем столбец, состоящий только из четных элементов;
				case 3: {
					boolean f = false;
					for (int j=0; j<n; j++) {
						boolean fl = true;
						for (int i=0; i<m; i++)
							fl = ar[i][j]%2 == 1 ? false : fl;
						if (fl)
							f =true;
					}		
					if (f)
						System.out.println("There is a column consisting even numbers");
					else
						System.out.println("There is not a column consisting even numbers");
					}
					break;
	// г) есть ли в нем столбец, в котором равное количество положительных
	// и отрицательных элементов;
				case 4: {
					boolean f = false;
					for (int j=0; j<n; j++) {
						int c = 0;
						for (int i=0; i<m; i++)
							if (ar[i][j] != 0)
								c = ar[i][j] > 0 ? c+1 : c-1;
						if (c == 0)
							f =true;
					}		
					if (f)
						System.out.println("There is a column consisting same number of positive and negative");
					else
						System.out.println("There is not a column consisting same number of positive and negative");
					}
					break;
	// д) есть ли в нем столбец, в котором имеются одинаковые элементы;
				case 5: {
					boolean f = false;
					five:for (int j=0; j<n; j++)
						for (int i=0; i<m; i++)
							for(int k=i+1; k<m; k++)	
								if (ar[i][j] == ar[k][j]) {
									f =true;
									break five;
								}
					if (f)
						System.out.println("There is a column consisting same elements");
					else
						System.out.println("There is not a column consisting same elements");
					}
					break;
	// е) есть ли в нем столбец, в котором имеются как минимум три элемента,
	// являющиеся минимальными в массиве.
				case 6: {
					boolean f = false;
					int min = ar[0][0];
					for (int j=0; j<n; j++)
						for (int i=0; i<m; i++)
							min = min > ar[i][j] ? ar[i][j] : min;
					for (int j=0; j<n; j++) {
						int c =0;
						for (int i=0; i<m; i++)
							c = ar[i][j] == min ? c+1 : c;
						if (c > 2) 
								f =true;
					}
					if (f)
						System.out.println("There is a column consisting 3 and more minimum elements");
					else
						System.out.println("There is not a column consisting 3 and more minimum elements");
					}
					break;
			}
		} while (o != 0);
	}
}