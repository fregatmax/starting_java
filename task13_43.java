/*
	известна информация о багаже (количество вещей и общий вес багажа)
	24-х пассажиров.
	а) Найти число пассажиров, имеющих более двух вещей.
	б) Выяснить, имеется ли хоть один пассажир, багаж которого состоит из одной вещи весом менее 25 кг.
	в) Найти число пассажиров, количество вещей которых превосходит среднее
	число вещей всех пассажиров.
	г) Найти номер багажа, в котором средний вес одной вещи отличается от
	общего среднего веса одной вещи не более чем на 0,5 кг.
*/

class Luggage13_43 {
	int amount;
	double weight;
	
	Luggage13_43() {
		java.util.Scanner in = new java.util.Scanner(System.in);
		System.out.println("----------------");
		System.out.print("Enter number of things > ");		
		this.amount = in.nextInt();
		System.out.print("Enter weight of luggage > ");
		this.weight = in.nextDouble();
	}
}
class task13_43 {
	public static void main(String[] args) {
		final int n = 6; // amount of passengers
		Luggage13_43[] ar = new Luggage13_43[n];
		for (int i=0; i<n; i++)
			ar[i] = new Luggage13_43();
		System.out.println("------------------");
		int a = 0;
		double avAmount = 0, avWeight = 0;
		boolean b = false;
		for (int i=0; i<n; i++) {
			if (ar[i].amount > 2)
				a++;
			if (ar[i].amount == 1 && ar[i].weight < 25)
				b = true;
			avAmount += ar[i].amount;
			avWeight += ar[i].weight;
		}
		avWeight /= avAmount;
		avAmount /= n;
		System.out.println("Amount of passengers with more than 2 items in luggage - "+a);
		if (b)
			System.out.println("There is a passenger with one item less than 25kg");
		int v = 0, g = 0; 
		for (int i=0; i<n; i++) {
			if (ar[i].amount > avAmount)
				v++;
			if (Math.abs(ar[i].weight/ar[i].amount - avWeight) <= 0.5)
				g = (i+1);
		}
		System.out.println("Amount of passengers with more than average amount of items - "+v);
		System.out.println("Number of luggage, where weight of on item differs from average less than 0.5 - "+g);
	}
}