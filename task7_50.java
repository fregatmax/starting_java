import java.util.Scanner;

class task7_50{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter number n > ");
	int n = stdin.nextInt(), order = 0, b;
	for(int i=1;i<=n;i++) {
		System.out.print("Enter b("+i+") > ");
		b = stdin.nextInt();
		if(b>100)	order = i;
	}
	System.out.println("Number of last number greater than 100 is "+order);
}
}