/*
	имеется текстовый файл. Все четные строки этого файла записать во второй
	файл, а нечетные — в третий файл. Порядок следования строк сохраняется.
*/

import java.util.Scanner;
import java.io.*;

class task15_29 {
	public static void main(String[] args)
	throws IOException
	{
		String temp;
		try {
			Scanner in = new Scanner(new File("task15_29_in.txt"));
			FileWriter fw2 = new FileWriter("task15_29_out2.txt");
			FileWriter fw3 = new FileWriter("task15_29_out3.txt");
			while (in.hasNextLine()) {
				fw3.write(in.nextLine()+"\n");
				if (in.hasNextLine())
					fw2.write(in.nextLine()+"\n");
			}
			fw2.close();
			fw3.close();
		}
		catch (Exception e) { System.out.println(e); }
	}
}