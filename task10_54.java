/*
	Написать рекурсивную процедуру перевода натурального
	числа из десятичной системы счисления в двоичную.
*/
import java.util.Scanner;

class task10_54{
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		System.out.print("Enter n > ");
		int n = in.nextInt();
		decToBin(n);
	}
	static void decToBin(int n) {
		if (n/2 == 0) {
			System.out.print(n%2);
			return;
		}
		decToBin(n/2);
		System.out.print(n%2);
		return;
	}
}