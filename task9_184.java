/*
	*Дан текст. Проверить, правильно ли в нем расставлены 
	круглые скобки (т. е. находится ли справа от каждой 
	открывающей скобки соответствующая ей закрывающая скобка,
	а слева от каждой закрывающей — соответствующая
	ей закрывающая). Предполагается, что внутри каждой пары
	скобок нет других скобок.
	Ответом должны служить слова да или нет.
*/
import java.util.Scanner;

class task9_184{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter text > ");
	String s1 = stdin.nextLine();
	for (int i=0; i<s1.length(); i++) {
		if (s1.charAt(i) == ')') {
			System.out.println("No");
			return;
		}
		if (s1.charAt(i) == '(')
			if (i == s1.length()-1) {
				System.out.println("No");
				return;
			}
			else for (int j=(i+1); j<s1.length();j++) {
				if (s1.charAt(j) == '(' || 
						j == s1.length()-1 && s1.charAt(j) != ')') {
					System.out.println("No");
					return;
				}
				if (s1.charAt(j) == ')') {
					i = j;
					break;
				}
			}
	}
	System.out.println("Yes");
}
}