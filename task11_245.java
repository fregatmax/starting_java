/*
	Дан массив. Переписать его элементы в другой массив такого же размера
	следующим образом: сначала должны идти все отрицательные элементы,
	а затем все остальные. использовать только один проход по исходному
	массиву.
*/
import java.util.Scanner;

class task11_245 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter length of the array >");
		int len = in.nextInt();
		int ar[] = new int[len];
		System.out.print("Enter array elements > ");
		for (int i=0; i<len; i++) {
			ar[i] = in.nextInt();
		}
		int[] ar1 = new int[ar.length];
		for (int i=0, j=0; i<ar.length; i++)
			if (ar[i] < 0)
				ar1[j++] = ar[i];
			else
				ar1[ar.length-j] = ar[i];
		System.out.print("1st: ");
		for (int i=0; i<len; i++) 
			System.out.print(ar[i]+" ");
		System.out.print("\n2nd: ");
		for (int i=0; i<ar1.length; i++) 
			System.out.print(ar1[i]+" ");
	}
}
