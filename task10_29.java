/*
	Составить программу для нахождения общего количества заданной буквы
	в трех заданных предложениях. (Определить функцию для расчета количест-
	ва некоторой буквы в предложении.)
*/
import java.util.Scanner;

class task10_29{
	public static void main(String[] args){
		Scanner stdin = new Scanner(System.in);
		String s[] = new String[3];
		for (int i=0; i<3; i++) {
			System.out.print("Enter sentence > ");
			s[i] = stdin.nextLine();
		}		
		System.out.print("Enter character > ");
		char c = stdin.next().charAt(0);
		System.out.print("Total amount of letters \""+c+"\" in sentences - ");
		System.out.print(amountOfC(s[0],c) + amountOfC(s[1],c) + amountOfC(s[2],c));	
	}
	static int amountOfC(String s, char c) {
		int r = 0;
		for (int i=0; i<s.length(); i++)
			if (s.charAt(i) == c)
				r++;
		return r;
		
	}
}
