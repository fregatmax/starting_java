// Выяснить, является ли заданное число m членом геометрической прогрессии,
// первый член которой равен g, а знаменатель —  z .
import java.util.Scanner;

class task6_79{
public static void main(String[] args){
	Scanner stdin = new Scanner(System.in);
	System.out.print("Enter m, g, z > ");
	double m = stdin.nextDouble(), g = stdin.nextDouble(), z = stdin.nextDouble();
	if(z>1.0)
		while(g<m)	{
			g *= z;
		}
	else
		while(g>m)	{
			g *= z;
		}
	if(g==m) 	System.out.println("Belongs to the sequence");
	else		System.out.println("Doesn't belong to the sequence");
}
}