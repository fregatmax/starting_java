/*
	Написать рекурсивную функцию, определяющую, является ли заданное
	натуральное число простым (простым называется натуральное число,
	большее 1, не имеющее других делителей, кроме единицы и самого себя)
*/
import java.util.Scanner;

class task10_56{
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		System.out.print("Enter n > ");
		int n = in.nextInt();
		if (isPrime(n,2))
			System.out.println("Prime");
		else
			System.out.println("Not prime");
	}
	static boolean isPrime(int n) {
		if (2 > n/2) 
			return true;
		return n%2 != 0 ? isPrime(n, 3) : false;
	}
	static boolean isPrime(int n, int del) {
		if (del > n/2) 
			return true;
		return n%del != 0 ? isPrime(n, ++del) : false;
	}
}