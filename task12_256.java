/*
	Дан двумерный массив из двенадцати строк и трех столбцов. Переставить
	строки так, чтобы они располагались следующим способом: первая,
	двенадцатая, вторая, одиннадцатая, ..., пятая, восьмая, шестая, седьмая.
*/

class task12_256 {
	public static int[][] rndMatrix(int n, int m) {
		java.util.Random rn = new java.util.Random();
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++)  {
				ar[i][j] = rn.nextInt(20);	
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		return ar;
	}
	public static void printMatrix(int[][] ar) {
		for (int i=0; i<ar.length; i++) {
			for (int j=0; j<ar[i].length; j++) 
				System.out.printf("%3d ",ar[i][j]);
			System.out.println();
		}
	}
	public static void main(String[] args) {
		final int n = 3, m = 12;
		int ar[][] = rndMatrix(n,m);
		for (int l=1; l<m; l+=2)
			for (int i=m-1; i>l; i--)
				for (int j=0; j<n; j++)
					ar[i][j] = ar[i-1][j] + ar[i][j] - (ar[i-1][j] = ar[i][j]);		
		System.out.println("-----------------");
		printMatrix(ar);
	}
}