/*
	Определить, имеются ли в одномерном массиве только два одинаковых
	элемента.
*/

import java.util.Scanner;

class task11_101 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter length of the array >");
		int len = in.nextInt();
		int ar[] = new int[len];
		System.out.print("Enter array elements > ");
		for (int i=0; i<len; i++) {
			ar[i] = in.nextInt();
		}
		int f = 0;
		for (int i=0; i<len; i++) 
			for (int j=i+1; j<len; j++)
				if (ar[i] == ar[j])
					f++;
		if (f == 1)
			System.out.println("Array contains one pair of same elements");
		else if(f == 0)
			System.out.println("Array doesn't contain same elements");
		else
			System.out.println("Array contains more than one pair of same elements");
	}
}