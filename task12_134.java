/*
	В двумерном массиве имеются только два одинаковых элемента. Найти их.
*/

import java.util.Scanner;

class task12_134 {
	public static int[][] rndMatrix(int n, int m) {
		java.util.Random rn = new java.util.Random();
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++)  {
				ar[i][j] = rn.nextInt(20);	
				System.out.printf("%3d ",ar[i][j]);
			}
			System.out.println();
		}
		return ar;
	}
	public static int[][] enterMatrix(int n, int m) {
		java.util.Scanner in = new java.util.Scanner(System.in);
		int ar[][] = new int[m][n];
		for (int i=0; i<m; i++) {
			System.out.print("Enter "+(i+1)+" row elements > ");
			for (int j=0; j<n; j++)
				ar[i][j] = in.nextInt();
			String temp = in.nextLine();
		}
		for (int i=0; i<m; i++) {
			for (int j=0; j<n; j++) 
				System.out.printf("%3d ",ar[i][j]);
			System.out.println();
		}
		return ar;
	}
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter n(width) and m(height) of 2D-array separated by space > ");
		final int n = in.nextInt(), m = in.nextInt();
		int ar[][] = enterMatrix(n,m);
		for (int i=0; i<m; i++) 
			for (int j=0; j<n; j++)
				for (int k=i; k<m; k++)
					for (int l=(k==i?(j+1):0); l<n; l++)
						if (ar[i][j] == ar[k][l]) {
							System.out.println("ar["+(i+1)+"]["+(j+1)+"]=ar["+(k+1)+"]["+(l+1)+"] = "+ar[i][j]);
							return;
							}			
	}
}